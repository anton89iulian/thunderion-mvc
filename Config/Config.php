<?php

namespace Thunderion\Config;

use Thunderion\Parameters;

class Config extends Parameters
{
    public function set( string $key, $value )
    {
        $this->_data[ $key ] = is_array( $value ) ? new self( $value ) : $value;
        return $this;
    }
    
    public function toArray( ) : array
    {
        $array = array( );
        
        foreach( $this->_data as $key =>  $value ) {
            $array[ $key ] = ( $value instanceof self ) ? $value->toArray( ) : $value;
        }
        
        return $array;
    }
    
    private function merge( self $a, array $b )
    {
        foreach( $b as $key => $value ) {
           
            if( null == $a->get( $key, null ) ) {
                $a->set( $key, $value );
            } else if( is_array( $value ) && ( $a->get( $key ) instanceof self ) ) {
                $this->merge( $a->get( $key ), $value );
            }
        }
        
        return $a;
    }
    
    public function mergeWith( array $array )
    {
        return $this->merge( $this, $array );
    }
}
