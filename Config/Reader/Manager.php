<?php

namespace Thunderion\Config\Reader;

use Thunderion\Service\Manager as AbstractManager;
use Thunderion\Config\Reader\ReaderInterface;
use Thunderion\Config\Exception\InvalidArgument as InvalidArgumentException;

class Manager extends AbstractManager
{
    public function __construct( ) 
    {
        $this->setNamespace( __NAMESPACE__ );
    }
    
    public function validate( $service ) 
    {
        if( $service instanceof ReaderInterface ) {
            return null;
        }
        
        throw new InvalidArgumentException( sprintf( 'Service is invalid; must implement %s\ReaderInterface', __NAMESPACE__ ) );
    }
}
