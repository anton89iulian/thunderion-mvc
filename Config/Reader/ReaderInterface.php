<?php

namespace Thunderion\Config\Reader;

interface ReaderInterface 
{
    public function fromFile( string $path ) : array;
    
    public function fromString( string $string ) : array;
}
