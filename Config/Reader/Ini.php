<?php

namespace Thunderion\Config\Reader;

use Thunderion\Config\Reader\ReaderInterface;
use Thunderion\Config\Exception\ErrorReadingFile as ErrorReadingFileException;
use Thunderion\Config\Exception\ErrorReadingString as ErrorReadingStringException;

class Ini implements ReaderInterface
{
    public function fromString( string $string ): array
    {
        if( empty( $string ) ) {
            return array( );
        }
        
        if( false === ( $ini = parse_ini_string( $string, true ) ) ) {
            throw new ErrorReadingStringException( );
        }
        
        return $this->processArray( $ini );
    }
    
    public function fromFile( string $path ): array
    {
        if( !is_file( $path ) || !is_readable( $path ) || false === ( $ini = parse_ini_file( $path, true ) ) ) {
            throw new ErrorReadingFileException( $path );
        }
        
        return $this->processArray( $ini );
    }
    
    private function processArray( array $data ) : array
    {
        $array = array( );
        foreach( $data as $key => $value ) {
            
            if( $key == '@include' ) {
                throw new \Exception( 'Cannot process @include statement' );
            }
            
            if( is_array( $value ) ) {
                $value = $this->processArray( $value );
            }
            
            if( false !== strpos( $key, '.' ) ) {
                $keys = explode( '.', $key );
                $array = array_merge_recursive( $array, $this->buildArray( $keys , $value ) );
            } else {
                $array[ $key ] = $value;
            }
        }
        
        return $array;
    }
    
    public function buildArray( $keys, $value )
    {
        if( 0 == count( $keys ) ) {
            return $value;
        }
        
        $array = array( );
        $first = array_shift( $keys );
        $array[ $first ] = $this->buildArray( $keys , $value );
        return $array;
    }
}
