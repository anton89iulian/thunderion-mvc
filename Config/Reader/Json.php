<?php

namespace Thunderion\Config\Reader;

use Thunderion\Config\Reader\ReaderInterface;
use Thunderion\Config\Exception\ErrorReadingFile as ErrorReadingFileException;
use Thunderion\Config\Exception\ErrorReadingString as ErrorReadingStringException;

class Json implements ReaderInterface
{
    public function fromString( $string ) : Array
    {
        if( empty( $string ) ) {
            return array( );
        }
        
        $data = json_decode( $string, true );
        
        if( json_last_error( ) !== JSON_ERROR_NONE ) {
            throw new ErrorReadingStringException(  );
        }
        
        return $data;
    }
    
    public function fromFile( string $path ) : Array
    {
        if( !is_file( $path ) || !is_readable( $path ) ) {
            throw new ErrorReadingFileException( $path );
        }
        
        return $this->fromString( trim( file_get_contents( $path ) ) );
    }
}
