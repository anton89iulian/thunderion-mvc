<?php

namespace Thunderion\Config;
use Thunderion\Config\Reader\Manager as ReaderManager;
use Thunderion\Config\Writer\Manager as WriterManager;
use Thunderion\Config\Exception\UnsupoportedConfigFileExtension as UnsupoportedConfigFileExtensionException;
use Thunderion\Config\Reader\ReaderInterface;
use Thunderion\Config\Writer\WriterInterface;
use Thunderion\Config\Exception\FileNotFound as FileNotFoundException;
use Thunderion\Config\Config;

class Factory 
{
    protected static $_reader_manager       = null;
    protected static $_writer_manager       = null;
    protected static $_reader_extensions    = array(
        'json' => 'json',
        'ini'  => 'ini'
    );
    protected static $_writer_extensions    = array(
        'json' => 'json',
        'ini'  => 'ini'
    );
    
    
    public static function setReaderManager( ReaderManager $manager )
    {
        static::$_reader_manager = $manager;
    }
    
    public static function getReaderManager(  ) : ReaderManager
    {
        if( null === static::$_reader_manager ) {
            static::setReaderManager( new ReaderManager( ) );
        }
        
        return static::$_reader_manager;
    }
    
    public static function setWriterManager( WriterManager $manager )
    {
        static::$_writer_manager = $manager;
    }
    
    public static function getWriterManager(  ) : WriterManager
    {
        if( null === static::$_writer_manager ) {
            static::setWriterManager( new WriterManager( ) );
        }
        
        return static::$_writer_manager;
    }
    
    public static function setReader( $extension, ReaderInterface $reader )
    {
        static::$_reader_extensions[ $extension ] = $reader;
    }
    
    public static function getReader( $extension )
    {
        if( !isset( static::$_reader_extensions[ $extension ] ) ) {
            throw new UnsupoportedConfigFileExtensionException( );
        }
        
        if( is_string( static::$_reader_extensions[ $extension ] ) ) {
            static::setReader( $extension, static::getReaderManager( )->get( static::$_reader_extensions[ $extension ] ) );
        }
        
        return static::$_reader_extensions[ $extension ];
    }
    
    public static function setWriter( $extension, WriterInterface $writer )
    {
        static::$_writer_extensions[ $extension ] = $writer;
    }
    
    public static function getWriter( $extension )
    {
        if( !isset( static::$_writer_extensions[ $extension ] ) ) {
            throw new UnsupoportedConfigFileExtensionException( );
        }
        
        if( is_string( static::$_writer_extensions[ $extension ] ) ) {
            return static::getWriterManager( )->get( static::$_writer_extensions[ $extension ] );
        }
        
        return static::$_writer_extensions[ $extension ];
    }
    
    public static function fromFile( string $path ) : Config
    {
        if( !file_exists( $path ) ) {
            throw new FileNotFoundException( );
        }
        
        if( !( $info = pathinfo( $path ) ) || !isset( $info[ 'extension' ] ) ) {
           throw new UnsupoportedConfigFileExtensionException( );
        }
        
        if( $info[ 'extension' ] == 'php' ) {
            $data = include $path;
            return new Config( !is_array( $data ) ? array( $data ) : $data );
        }
        
        return new Config( static::getReader( $info[ 'extension' ] )->fromFile( $path ) );
    }
    
    public static function toFile( $config, string $path ) : string
    {
        if( !( $info = pathinfo( $path ) ) || !isset( $info[ 'extension' ] ) ) {
           throw new UnsupoportedConfigFileExtensionException( );
        }
        
        return static::getWriter( $info[ 'extension' ] )->toFile( $config, $path );
    }
}
