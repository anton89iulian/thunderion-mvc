<?php

namespace Thunderion\Config\Writer;

use Thunderion\Config\Writer\WriterInterface;
use Thunderion\Config\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Config\Exception\ErrorWritingFile as ErrorWritingFileException;
use Thunderion\Config\Config;

class Ini implements WriterInterface
{   
    private function processArray( array $array ) : string
    {
        $string = "";
        
        foreach( $array as $key => $value ) {
            
            if( is_array( $value ) ) {
                $string .= "[{$key}]" . PHP_EOL;
                $string .= $this->processArray( $value );
            } else {
                $string .= "{$key} = \"{$value}\"" . PHP_EOL;
            } 
        }
        
        return $string;
    }
    
    private function formatArray( array $array, string $section_key = null, array &$formated = array( )  ) : array
    {
        foreach( $array as $key => $value ) {
            if( is_string( $value ) ) {
                null === $section_key ? $formated[ $key ] = $value : $formated[ $section_key ][ $key ] = $value;
            } else if( is_array( $value ) ) {
                $this->formatArray( $value, null === $section_key ? $key : $section_key .'.' . $key , $formated );
            } else {
                throw new InvalidArgumentException( );
            }
        }

        return $formated;
    }
    
    public function toString( $config ): string 
    {
        if( $config instanceof Config ) {
            $config = $config->toArray( );
        } else if( !is_array( $config ) ) {
            throw new InvalidArgumentException( );
        }
        
        return $this->processArray( $this->formatArray( $config ) ); 
    }
    
    public function toFile( $config, string $path): string 
    {
        if( empty( $path ) || ( file_exists( $path ) && !is_writable( $path ) ) ) {
            throw new ErrorWritingFileException( $path );
        }
        
        $string = $this->toString( $config );
        
        if( false === file_put_contents( $path, $string ) ) {
            throw new ErrorWritingFileException( $path );
        }
        
        return $string;
    }
    
}

