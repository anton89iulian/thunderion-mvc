<?php

namespace Thunderion\Config\Writer;

use Thunderion\Config\Writer\WriterInterface;
use Thunderion\Config\Config;
use Thunderion\Config\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Config\Exception\ErrorWritingFile as ErrorWritingFileException;

class Json implements WriterInterface 
{
    public function toString( $config  ): string 
    {
        return $this->process( $config );
    }
    
    public function toFile( $config, string $path ) : string
    {
        if( empty( $path ) || ( file_exists( $path ) && !is_writable( $path ) ) ) {
            throw new ErrorWritingFileException( $path );
        }
        
        $string = $this->process( $config, JSON_PRETTY_PRINT );
        
        if( false === file_put_contents( $path, $string ) ) {
            throw new ErrorWritingFileException( $path );
        }
        
        return $string;
    }
    
    private function process( $config, int $option = 0 ) : string
    {
        if( $config instanceof Config ) {
            $config = $config->toArray( );
        } else if( !is_array( $config ) ) {
            throw new InvalidArgumentException( );
        }
        
        return json_encode( $config, $option );
    }
}
