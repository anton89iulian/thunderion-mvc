<?php

namespace Thunderion\Config\Writer;

use Thunderion\Service\Manager as AbstractManager;
use Thunderion\Config\Writer\WriterInterface;
use Thunderion\Config\Exception\InvalidArgument as InvalidArgumentException;

class Manager extends AbstractManager
{
    public function __construct( ) 
    {
        $this->setNamespace( __NAMESPACE__ );
    }
    
    public function validate( $service ) 
    {
        if( $service instanceof WriterInterface ) {
            return null;
        }
        
        throw new InvalidArgumentException( sprintf( 'Service is invalid; must implement %s\WriterInterface', __NAMESPACE__ ) );
    }
}
