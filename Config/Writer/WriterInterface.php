<?php

namespace Thunderion\Config\Writer;

interface WriterInterface 
{
    public function toFile( $config, string $path ) : string;
    public function toString( $config ) : string;
}
