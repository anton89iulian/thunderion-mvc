<?php

namespace Thunderion\Http;

class Client 
{
    protected $_output      = null;
    protected $_http_code   = null;
    protected $_error       = false;
    protected $_curl        = null;
    protected $_params      = array();
    protected $_body        = '';
    protected $_options     = array(
        CURLOPT_CONNECTTIMEOUT  => 30,
        CURLOPT_TIMEOUT         => 30,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_HEADER          => false,
        CURLOPT_HTTPHEADER      => array( )
    );
    
    public function addHeader( $key, $value = null )
    {
        $this->_options[ CURLOPT_HTTPHEADER ][ ] = $key . ': ' . $value;
        return $this;
    }
    
    public function addHeaders ( array $headers )
    {
        foreach( $headers as $key => $value )
        {
            $this->addHeader($key, $value);
        }
        return $this;
    }
    
    public function setUserAgent( $user_agent )
    {
        $this->_options[CURLOPT_USERAGENT] = $user_agent;
        return $this;
    }
    
    public function setParams( array $params ) 
    {
        foreach( $params as $key => $value ) {
            $this->setParam( $key, $value );
        }
        return $this;
    }
    public function setOption( $key, $value )
    {
        $this->_options[$key] = $value;
        return $this;
    }
    public function setParam( $key, $value )
    {
        $this->_params[$key] = $value;
        return $this;
    }
    
    public function setBody( $string )
    {
        $this->_body = $string;
        return $this;
    }
    
    public function getBody( )
    {
        return $this->_body;
    }
    
    public function openConnection( $url, $method = 'GET' ) 
    {
        switch( $method ) {
            case 'GET':
                if( empty( $this->_body ) ) {
                    break;
                }
                $url .= ( strpos( $url, '?' ) !== false ? '&' : '?' ) . $this->getBody( );
                break;
            case 'POST':
                $this->setOption( CURLOPT_POST, 1 );
                $this->setOption(CURLOPT_POSTFIELDS,  $this->getBody( ) );
                break;
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
                $this->setOption(CURLOPT_CUSTOMREQUEST, $method);
                $this->setOption(CURLOPT_POSTFIELDS,  $this->getBody( ) );
                break;
            default:
                return false;  
        }
        
        $this->_curl = curl_init( $url );
        curl_setopt_array( $this->_curl, $this->_options );

        return $this;
    }
    public function exec(  )
    {
        if( null === $this->_curl ) {
            return $this;
        }
        $this->_output = curl_exec( $this->_curl );
        $this->_error = curl_error( $this->_curl );
        $this->_http_code = curl_getinfo( $this->_curl, CURLINFO_HTTP_CODE);
        return $this;
    }
    public function closeConnection(  )
    {
        if( null !== $this->_curl ) {
            curl_close( $this->_curl );
            $this->_curl = null;
        }
        $this->_params = array();
        return $this;
    }
    public function send( $url, $method = 'GET', $params = null ) 
    {   
        if( null !== $params && is_array( $params )) {
            $this->setParams( $params );
            $this->setBody( http_build_query($this->_params) );
        } else if( null !== $params && is_string( $params ) ) {
            $this->setBody( $params );
        }
           
        $this->openConnection( $url, $method )
             ->exec( )
             ->closeConnection();
        return $this;
    }
    public function getRawOutput(   )
    {
        return $this->_output;
    }
    public function getError(  )
    {
        return $this->_error;
    }
    public function getResponseCode(  )
    {
        return $this->_http_code;
    }

}


