<?php

namespace Thunderion\Http;

use Thunderion\Http\Exception\HeadersAlreadySent as HeadersAlreadySentException;
use Thunderion\Http\Headers as HttpHeaders;

class Response 
{
    const HTTP_VERSION_10 = 'HTTP/1.0';
    const HTTP_VERSION_11 = 'HTTP/1.1';
    
    
    protected $_headers         = null;
    protected $_status_code     = 200;
    protected $_reason_phrase   = 'OK';
    protected $_body            = null;
    protected $_http_version    = self::HTTP_VERSION_11;
    protected $_reason_phrases  = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy', 
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    );
    
    public function __construct( string $body = null, int $statusCode = null, array $headers = null ) 
    {
        if( null !== $body ) {
            $this->setBody( $body );
        }
        
        if( null !== $statusCode ) {
            $this->setStatusCode( $statusCode );
        }
        
        if( null !== $headers ) {
            $this->addHeaders( $headers );
        }
    }
    
    public function setBody( string $string ) : self
    {
        $this->_body = $string;
        return $this;
    }
    
    public function getBody( ) : string
    {
        return $this->_body;
    }
    
    public function statusCodeExists( int $code ) : bool
    {
        return array_key_exists( $code, $this->_reason_phrases );
    }
    
    public function setStatusCode( int $code ) : self
    {
        $this->_status_code = $code;
        
        if( $this->statusCodeExists( $code ) ) {
            $this->setReasonPhrase( $this->_reason_phrases[ $code ] );
        }
        
        return $this;
    }
    
    public function getStatusCode( ) : int
    {
        return $this->_status_code;
    }
    
    public function setReasonPhrase( string $phrase ) : self
    {
        $this->_reason_phrase = $phrase;
        return $this;
    }
    
    public function getReasonPhrase(  ) : string
    {
        return $this->_reason_phrase;
    }
    
    public function setHeaders( HttpHeaders $headers ) : self
    {
        $this->_headers = $headers;
        return $this;
    }
    
    public function addHeader( string $name, $value, $replace = false ) : self
    {
        $this->getHeader( )->add( $name, $value, $replace );
        return $this;
    }
    
    public function addHeaders( array $headers, $replace = false ) : self
    {
        foreach( $headers as $name => $value ) {
            $this->addHeader( $name, $value, $replace );
        }
        
        return $this;
    }
    
    public function getHeaders( ) : HttpHeaders
    {
        if( null === $this->_headers ) {
            $this->setHeaders( new HttpHeaders( ) );
        }
        
        return $this->_headers;
    }
    
    public function getHeader( string $key = null, string $default = null )
    {
        
        if( null === $key ) {
            return $this->getHeaders( );
        }
        
        return $this->getHeaders( )->get( $key, $default );
    }
    
    public function setHttpVersion( string $version ) : self
    {
        $this->_http_version = $version;
        return $this;
    }
    
    public function getHttpVersion( ) : string
    {
        return $this->_http_version;
    }
    
    public function renderStatusLine( )
    {
        return trim( sprintf( "%s %d %s", $this->getHttpVersion( ), $this->getStatusCode( ), $this->getReasonPhrase( ) ) );
    }
    
    public function sendStatus(  ) : self
    {
        if( headers_sent( ) ) {
            throw new HeadersAlreadySentException( );
        }
        
        header( $this->renderStatusLine( ), true );
        
        return $this;
    }
    
    protected function deepReplace( $search, string $string ) : string
    {
        $count = 1;
        
        while ( $count ) {
            $string = str_replace( $search, '', $string, $count );
        }
        
        return $string;
    }
    
    protected function sanitizeRedirect( $location ) 
    { 
        $location = preg_replace( '|[^a-z0-9-~+_.?#=&;,/:%!*@]|i', '', $location );
        $location = preg_replace( '/[\x00-\x08\x0B\x0C\x0E-\x1F]/', '', $location );
        $location = preg_replace( '/(\\\\0)+/', '', $location );
        $strip = array( '%0d', '%0a', '%0D', '%0A' );
        
        return $this->deepReplace( $strip, $location );
    }
    
    public function redirect( string $url, int $code = 302 , int $timeout = 0 )
    {
        if( $timeout > 0 ) {
           $this->setStatusCode( $code )
                ->addHeader( 'Refresh', array( $timeout, 'URL=' . $this->sanitizeRedirect( $url ) ) );
           return $this;
        }
             
        header( "Location:  {$this->sanitizeRedirect( $url )}" , true, $code );
        die();
    }
    
    public function dispatch( ) : self
    {
        $this->sendStatus( );
        $this->getHeaders( )->send( );
        echo $this->getBody( );
        return $this;
    }
    
    public function toString( ) : string
    {
        return $this->renderStatusLine( ) . "\r\n" .( (string) $this->getHeaders( ) ) . "\r\n" . $this->getBody( );
    }
    
    public function __toString( ) 
    {
        return $this->toString( );
    }
}
