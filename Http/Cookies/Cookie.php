<?php

namespace Thunderion\Http\Cookies;

use Thunderion\Http\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Parameters;

class Cookie 
{
    protected $_parameters = null;
    
    public function __construct( Parameters $params ) 
    {
        if( null !== $params ) {
            $this->setParams( $params );
        }
    }
    
    public function setParams( $params ) : self
    {
        if( is_array( $params ) ) {
            $params = new Parameters( $params );
        } else if( !( $params instanceof Parameters ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams( ) : Parameters
    {
        return $this->_parameters;
    }

    public function setName( string $name ) : self
    {
        if( empty( $name ) || !( preg_match( '/^[a-zA-Z0-9_]+$/', $name ) ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->getParams( )->set( 'name', $name );
        return $this;
    }
    
    public function getName(  ) : string
    {
        return $this->getParams( )->get( 'name' );
    }
    
    public function setValue( $value ) : self
    {
        $this->getParams( )->set( 'value', $value );
        return $this;
    }
    
    public function getValue( $default = null )
    {
        return $this->getParams( )->get( 'value', $default );
    }
    
    public function setExpires( $expires ) : self
    {
        $expires = !is_numeric( $expires ) ? strtotime( $expires ) : (int) $expires;
        
        if( $expires == 0 ) {
            return $this;
        }
        
        $this->getParams( )->set( 'expires', $expires );
        return $this;
    }
    
    public function getExpires( ) : int
    {
        return $this->getParams( )->getInt( 'expires', 0 );
    }
    
    public function setExpiresIn( int $seconds ) : self
    {
        if( $seconds == 0 ) {
            return $this;
        }
        
        $this->getParams( )->set( 'expires', time( ) + $seconds ); 
        return $this;
    }
    
    public function setDomain( string $domain ) : self
    {
        $this->getParams( )->set( 'domain', rtrim( $domain, '\/' ) );
        return $this;
    }
    
    public function getDomain( ) : string
    {
        return $this->getParams( )->get( 'domain' );
    }
    
    public function setPath( $path ) : self
    {
        $this->getParams( )->set( 'path', $path );
        return $this;
    }
    
    public function getPath(  ) : string
    {
        return $this->getParams( )->get( 'path', '/' ) ;
    }
    
    public function setSecure( bool $secure ) : self
    {
        $this->getParams( )->set( 'secure', $secure );
        return $this;
    }
    
    public function isSecure( ) : bool
    {
        return $this->getParams( )->getBool( 'expires', false );
    }
    
    public function setHttpOnly( bool $only ) : self
    {
        $this->getParams( )->set( 'http_only', $only );
        return $this;
    }
    
    public function isHttpOnly( ) : bool
    {
        return $this->getParams( )->getBool( 'http_only', false );
    }

    public function __sleep( ) 
    {
        return array( '_parameters' );
    }
}
