<?php

namespace Thunderion\Http\Cookies;
use Thunderion\Parameters;
use Thunderion\Http\Cookies\Cookie as HttpCookie;

class Handler 
{
    protected $_parameters  = null;
    
    public function setParams( Parameters $params ) : self
    {
        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams(  ) : Parameters
    {
        return $this->_parameters;
    }
    
    public function get( string $name ) : HttpCookie
    {
        return $this->getParams( )->get( $name, new HttpCookie( new Parameters( array( 'name' => $name ) ) ) );
    }
    
    public function set( string $name, $values, $expres, $domain, $path, $secure, $htp )
    {
        
    }
    
    private function render( HttpCookie $cookie ) : string
    {
        $string = $cookie->getName( ) .'="' . serialize( $cookie ) .'"' ;
        
        if( $cookie->getExpires( ) != 0 ) {
            $string .= gmdate( '; Expires= D, d M Y H:i:s GMT' );
        }
        
        if( $cookie->getDomain( ) ) {
            $string .= "; Domain=" . $cookie->getDomain( );
        }
        
        if( $cookie->getPath( ) ) {
            $string .= "; Path=" . $cookie->getDomain( );
        }
        
        if( $cookie->isSecure( ) ) {
            $string .= '; Secure';
        }
        
        if( $cookie->isHttpOnly( ) ) {
            $string .= '; HttpOnly';
        }
        
        return $string;
    }
    
    public function toString( ) : string
    {
        if( empty( $this->getParams( ) ) ) {
            return '';
        }
        
        
        $string = 'Set-Cookie: ';
        
        foreach( $this->getParams( ) as $cookie ) {
            $string .= $this->render( $cookie ) . ', ';
        }
        
        return rtrim( $string, ', ' );
    }
    
    public function __toString( ) 
    {
        return $this->toString( );
    }
    
    public function send( )
    {
        
    }
}
