<?php

namespace Thunderion\Http;

use Thunderion\Parameters;
use Thunderion\Http\Headers as HttpHeaders;

class Request 
{
    const METHOD_OPTIONS            = 'OPTIONS';
    const METHOD_GET                = 'GET';
    const METHOD_HEAD               = 'HEAD';
    const METHOD_POST               = 'POST';
    const METHOD_PUT                = 'PUT';
    const METHOD_DELETE             = 'DELETE';
    const METHOD_TRACE              = 'TRACE';
    const METHOD_CONNECT            = 'CONNECT';
    const METHOD_PATCH              = 'PATCH';
    const METHOD_PROPFIND           = 'PROPFIND';
    
    protected $_post_parameters     = null;
    protected $_query_parameters    = null;
    protected $_files_parameters    = null;
    protected $_server_parameters   = null;
    protected $_cookies_parameters  = null;
    protected $_method              = null;
    protected $_http_uri            = null;
    protected $_client_ip           = null;
    protected $_host                = null;
    protected $_domain              = null;
    protected $_subdomain           = null;
    protected $_base_url            = null;
    protected $_input_parameters    = null;
    protected $_body                = null;
    protected $_secure              = null;
    protected $_schema              = null;
    protected $_script_name         = null;
    protected $_protocol            = null;
    protected $_headers             = null;
    
    public static function fromPlatform( ) : self
    {
        $obj = new self( );
       
        if( !empty( $_GET ) ) {
            $obj->setQuery( new Parameters( $_GET ) );
        }
        
        if( !empty( $_POST ) ) {
            $obj->setPost( new Parameters( $_POST ) );
        }
        
        if( !empty( $_FILES ) ) {
            $obj->setFiles( new Parameters( $_FILES ) );
        }
        
        if( !empty( $_SERVER ) ) {
            $obj->setServer( new Parameters( $_SERVER ) );
            $headers = new HttpHeaders( );
        
            foreach( $_SERVER as $name => $value ) {
                if( substr( $name, 0, 5 ) !== 'HTTP_' ) {
                    continue;
                }

                $hName = str_replace( ' ', '-', ucwords( strtolower( str_replace( '_', ' ', substr( $name, 5 ) ) ) ) );
                $headers->add( $hName, $value );
            }
            
            $obj->setHeaders( $headers );
        }
        
        if( !empty( $_COOKIE ) ) {
            $obj->setCookies( new Parameters( $_COOKIE ) );
        }
        
        
        
        $obj->setBody( file_get_contents('php://input') );
        
        $result = array( );
        
        if( $obj->getHeader( 'Content-Type' ) === 'application/json' ) {
            $result = json_decode( $obj->getBody( ), true );
        } else  {
            parse_str( $obj->getBody( ), $result );
        }
        
        if( !empty( $result ) ) {
            $obj->setInput( new Parameters( $result ) );
        }
        
        if( ( ( ( $https = $obj->getServer( 'HTTPS' , false) ) && ( $https == 0 || $https == 'on' ) ) || $obj->getServer( 'HTTP_X_FORWARDED_PROTO' ) == 'https' ) ) {
            $obj->setSecure( true )
                ->setSchema( 'https' );
                    
        } else {
            $obj->setSecure( false )
                ->setSchema( 'http' );
        }
        
        return $obj;

    }
    
    public function setBody( string $body ) : self
    {
        $this->_body = $body;
        return $this;
    }
    
    public function getBody( ) : string
    {
        return $this->_body;
    }
    
    public function setSecure( bool $secure ) : self
    {
        $this->_secure = $secure;
        return $this;
    }
    
    public function isSecure(  ) : bool
    {
        return $this->_secure;
    }
    
    public function setSchema( string $schema ) : self
    {
        $this->_schema = strtolower( $schema );
        return $this;
    }
    
    public function getSchema( ) : string
    {
        return $this->_schema;
    }
    
    public function setScriptName( string $path ) : self
    {
        $this->_script_name = $path;
        return $this;
    }
    
    public function getScriptName( ) : string
    {
        if( null === $this->_script_name ) {
            $this->setScriptName( $this->detectScriptName( ) );
        }
        
        return $this->_script_name;
    }
    
    public function setUri( string $uri ) : self
    {
        $this->_http_uri = $uri;
        return $this;
    }
    
    public function getUri( ) : string
    {
        if( null === $this->_http_uri ) {
            $this->setUri( $this->detectUri( ) );
        }
        
        return $this->_http_uri;
    }
    
    public function setHost( string $host ) : self
    {
        $this->_host = $host;
        return $this;
    }
    
    public function getHost(  ) : string
    {
        if( null === $this->_host ) {
            $this->setHost( $this->detectHost( ) );
        }
        
        return $this->_host;
    }
    
    public function setDomain( string $domain ) : self
    {
        $this->_domain = $domain;
        return $this;
    }
    
    public function getDomain( ) : string
    {
        if( null === $this->_domain ) {
            $this->setDomain( $this->detectDomain( ) );
        }
        
        return $this->_domain;
    }
    
    public function setSubdomain( string $subdomain ) : self
    {
        $this->_subdomain = $subdomain;
        return $this;
    }
    
    public function getSubdomain( ) : string
    {
        if( null === $this->_subdomain ) {
            $this->setSubdomain( $this->detectSubdomain( ) );
        }
        
        return $this->_subdomain;
    }
    
    public function setBaseUrl( string $url ) : self
    {
        $this->_base_url = $url;
        return $this;
    }
    
    public function getBaseUrl( ): string
    {
        if( null === $this->_base_url ) {
            $this->setBaseUrl( $this->getSchema( ) . '://' . $this->getHost( ) . $this->getScriptName( ) );
        }
        return $this->_base_url;
    }
    
    public function setServerProtocol( string $protocol ) : self
    {
        $this->_protocol = $protocol;
        return $this;
    }
    
    public function getServerProtocol(  ) : string
    {
        if( null === $this->_protocol ) {
            $this->setServerProtocol( $this->getServer( 'SERVER_PROTOCOl', 'HTTP/1.0' ) );
        }
        
        return $this->_protocol;
    } 
    
    public function setInput( Parameters $params ) : self
    {
        $this->_input_parameters = $params;
        return $this;
    }
    
    public function getInput( string $key = null, $default = null )
    {
        if( null === $this->_input_parameters ) {
            $this->setInput( new Parameters( ) );
        }
        
        if( null === $key ) {
            return $this->_input_parameters;
        }
        
        return $this->_input_parameters->get( $key, $default ); 
    }
    
    public function setPost( Parameters $params ) : self
    {
        $this->_post_parameters = $params;
        return $this;
    }
    
    public function getPost( string $key = null, $default = null )
    {
        if( null === $this->_post_parameters ) {
            $this->setPost( new Parameters( ) );
        }
        
        if( null === $key ) {
            return $this->_post_parameters;
        }
        
        return $this->_post_parameters->get( $key, $default );
    }
    
    public function setQuery( Parameters $params ) : self 
    {
        $this->_query_parameters = $params;
        return $this;
    }
    
    public function getQuery( string $key = null, $default = null )
    {
        if( null === $this->_query_parameters ) {
            $this->setQuery( new Parameters( ) );
        }
        
        if( null === $key ) {
            return $this->_query_parameters;
        }
        
        return $this->_query_parameters->get( $key, $default );
    }
 
    public function setFiles(Parameters $params ) : self 
    {
        $this->_files_parameters = $params;
        return $this;
    }
    
    private function mapFilesParam( array $params ) : array
    {
        $array = array( );
        
        foreach( $params as $paramName => $data ) {
            if( !is_array( $data ) ) {
                return $params;
            }
            
            foreach( $data as $key => $value ) {
                $array[ $key ][ $paramName ] = $value;
            }
        }
        
        return $array;
    }
    
    public function mapFiles( array $files ) : array
    {
        $array = array( );
        
        foreach( $files as $name => $params ) {
            $array[ $name ] = $this->mapFilesParam( $params );
        }
        
        return $array;
    }
    
    public function getFiles( string $key, $default = null )
    {
        if( null === $this->_files_parameters ) {
            $this->setFiles( new Parameters( ) );
        }
        
        if( null === $key ) {
            return $this->_files_parameters;
        }
        
        return $this->_files_parameters->get( $key, $default );
    }
    
    public function setServer( Parameters $params ) : self
    {
        $this->_server_parameters = $params;
        return $this;
    }
    
    public function getServer( $key = null, $default = null )
    {
        if( null === $this->_server_parameters ) {
            $this->setServer( new Parameters( ) );
        }
        
        if( null === $key ) {
            return $this->_server_parameters;
        }
        
        return $this->_server_parameters->get( $key, $default );
    }
    
    public function setHeaders( HttpHeaders $headers ) : self
    {
        $this->_headers = $headers;
        return $this;
    }
    
    public function getHeaders( ) : HttpHeaders
    {
        if( null === $this->_headers ) {
            $this->setHeaders( new HttpHeaders( ) );
        }
        
        return $this->_headers;
    }
    
    public function getHeader( string $key, $default = null )
    {
        return $this->getHeaders( )->get( $key, $default );
    }
    
    public function setCookies( Parameters $params ) : self
    {
        $this->_cookies_parameters = $params;
        return $this;
    }
    
    public function getCookies( $key = null, $default = null )
    {
        if( null === $this->_cookies_parameters ) {
            $this->setCookies( new Parameters( ) );
        }
        
        if( null === $key ) {
            return $this->_cookies_parameters;
        }
        
        return $this->_cookies_parameters->get( $key, $default );
        
    }

    public function setMethod( string $method ): self
    {
        $this->_method = strtoupper( $method );
        return $this;
    }
    
    public function getMethod( ) : string
    {
        if( null === $this->_method ) {
            $this->setMethod( $this->getServer( 'REQUEST_METHOD', self::METHOD_GET ) );
        }
        
        return $this->_method;
    }
    
    public function isMethod( $method ) : bool
    {
        return ( $this->getMethod( ) === strtoupper( $method ) );
    }
    
    public function isGet( ) : bool
    {
        return $this->isMethod( self::METHOD_GET );
    }
    
    public function isPost( ) : bool
    {
        return $this->isMethod( self::METHOD_POST );
    }
    
    public function isPut( ) : bool
    {
        return $this->isMethod( self::METHOD_PUT );
    }
    
    public function isDelete( ) : bool
    {
        return $this->isMethod( self::METHOD_DELETE );
    }
    
    public function isOptions( ) : bool
    {
        return $this->isMethod( self::METHOD_OPTIONS );
    }
    
    public function isConnect( ) : bool
    {
        return $this->isMethod( self::METHOD_CONNECT );
    }
    
    public function isHead( ) : bool
    {
        return $this->isMethod( self::METHOD_HEAD );
    }
    
    public function isPatch( ) : bool
    {
        return $this->isMethod( self::METHOD_PATCH );
    }
    
    public function isTrace( ) : bool
    {
        return $this->isMethod( self::METHOD_TRACE );
    }
    
    public function isXMLHttpRequest( ) : bool
    {
        return ( 'XMLHttpRequest' === $this->getHeader( 'X_REQUESTED_WITH' ) );
    }
    
    public function setClientIp( $ip ) : self
    {
        $this->_client_ip = (string) $ip;
        return $this;
    }
    
    public function getClientIp( ) : string
    {
        if( null === $this->_client_ip && ( $ips = $this->detectClientIps( ) ) && !empty( $ips ) ) {
            $this->setClientIp( $ips[0] );
        }
        
        return $this->_client_ip;
    }
    
    
    private function detectScriptName( ) : string
    {
        $filename = null !== $this->getServer( 'SCRIPT_FILENAME') ? basename( $this->getServer( 'SCRIPT_FILENAME') ) : '';

        if( basename( $this->getServer( 'SCRIPT_NAME' ) ) === $filename ) {
            $baseUrl = $this->getServer( 'SCRIPT_NAME' );
        } else if( basename( $this->getServer( 'PHP_SELF' ) ) === $filename ) {
            $baseUrl = $this->getServer( 'PHP_SELF' );
        } else if ( basename( $this->getServer( 'ORIG_SCRIPT_NAME' ) ) === $filename ) {
            $baseUrl = $this->getServer( 'ORIG_SCRIPT_NAME' );
        } else {
            $path = $this->getServer( 'PHP_SELF', '' );
            $segs = array_reverse( explode( '/', trim( $filename, '/' ) ) );
            $index = 0;
            $last = count( $segs );
             $baseUrl = '';
            do {
                $seg = $segs[$index];
                $baseUrl = '/' . $seg . $baseUrl;
                ++$index;
            } while ( ($last > $index) && (false !== ( $pos = strpos($path, $baseUrl ) ) ) && (0 != $pos) );
        }
        
        return rtrim( str_replace( pathinfo( $baseUrl, PATHINFO_BASENAME ), '', $baseUrl ), '/' );
    }
    
    private function detectUri( ) : string
    {
        $uri = null;

        if( null !== ($httpXRewriteUrl = $this->getServer( 'HTTP_X_REWRITE_URL' ) ) ) {
            $uri = $httpXRewriteUrl;
        }

        if( null !== ( $httpXOriginalUrl = $this->getServer( 'HTTP_X_ORIGINAL_URL' ) ) ) {
            $uri = $httpXOriginalUrl;
        }

        $unecodedUrl = $this->getServer( 'UNENCODED_URL', '' );

        if( '1' == $this->getServer( 'IIS_WasUrlRewritten' ) && '' !== $unecodedUrl ) {
            return $unecodedUrl;
        }

        if( !$httpXRewriteUrl ) {
            $uri  = $this->getServer( 'REQUEST_URI' );
        }

        if ( null !== $uri ) {
            return preg_replace('#^[^/:]+://[^/]+#', '', $uri );
        }

        if( null !== ( $uri = $this->getServer('ORIG_PATH_INFO') ) ) {
            return $uri . ( ( $query = $this->getServer( 'QUERY_STRING', '' ) ) ? '?' . $query : '' );
        }

        return '/';
    }
    
    private function detectHost( ) : string
    {
        $sources = array('HTTP_X_FORWARDED_HOST', 'HTTP_HOST', 'SERVER_NAME', 'SERVER_ADDR');

        $host = null;

        foreach( $sources as $source ) {

            if( !empty( $host ) ) {
                break;
            }

            if( !( $host = $this->getServer( $source ) ) ) {
                continue;
            }
            if( $source === 'HTTP_X_FORWARDED_HOST' ) {
                $host = strtok( $host, ',' );
            }
        }

        return trim( preg_replace( '/:\d+$/', '', $host ) ) ;
    }
    
    private function filterClientIps( array $ips ) : array
    {
        foreach( $ips as $key => $ip ) {

            if (preg_match('{((?:\d+\.){3}\d+)\:\d+}', $ip, $match)) {
                $ip[$key] = $ip = $match[1];
            }

            if ( !filter_var( $ip, FILTER_VALIDATE_IP ) ) {
                unset($ips[$key]);
            }

        }

        return $ips ? array_reverse( $ips ) : array( );
    }
    
    private function detectClientIps( ) : array
    {
        $ips = array( );
        $ip = $this->getServer( 'REMOTE_ADDR' );

        $hasFwdHeader = $this->getServer(  )->has( 'HTTP_FORWARDED' );
        $hasXFwdHeader = $this->getServer(  )->has( 'HTTP_X_FORWARDED_FOR' );

        if( $hasFwdHeader ) {
            preg_match_all( '{(for)=("?\[?)([a-z0-9\.:_\-/]*)}', $this->getServer( )->get( 'HTTP_FORWARDED' ), $matches );
            $matches[3][] = $ip;
            $ips = $fwips = $this->filterClientIps( $matches[3] );
        }

        if( $hasXFwdHeader ) {
            $xfwips = array_map( 'trim', explode(',', $this->getServer( )->get( 'HTTP_X_FORWARDED_FOR' ) ) );
            $ips = $xfwips = $this->filterClientIps( $xfwips );
        }

        if( $hasXFwdHeader && $hasFwdHeader && $fwips !== $xfwips ) {
            throw new \Exception( 'Header conflict' );
        }

        return empty( $ips ) ? $this->filterClientIps( array( $ip ) ) : $ips;
    }
    
    private function detectDomain( )
    {
        $host = $this->getHost( );
        return ( preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i", $host, $matches) ? $matches['domain'] : $host );
    }
    
    private function detectSubdomain( )
    {
        return strstr( $this->getHost( ),  '.' .  $this->getDomain( ), true );
    }
    
}
