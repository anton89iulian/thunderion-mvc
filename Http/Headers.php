<?php

namespace Thunderion\Http;

use Thunderion\Parameters;
use Thunderion\Http\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Http\Exception\HeadersAlreadySent as HeadersAlreadySentException;

class Headers
{
    protected $_parameters = null;
    
    public function setParams( Parameters $params )
    {
        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams( ) : Parameters
    {
        if( null === $this->_parameters ) {
            $this->setParams( new Parameters( ) );
        }
        
        return $this->_parameters;
    }
    
    public function add( string $name, $values, $replace = false ) : self
    {
        if( empty( $name ) || ( !is_string( $values ) && !is_numeric( $values ) && !is_array( $values ) ) ) {
            throw new InvalidArgumentException( );
        }
        
        if( is_array( $values ) ) {
            $values = implode(',', $values );
        }
        
        if( $replace || !$this->getParams( )->has( $name ) ) {
            $this->getParams( )->set( $name, $values );
        } else {
            $this->getParams( )->set( $name, $this->getParams( )->get( $name, '' ) . ',' , $values );
        }
        
        return $this;
    }
    
    public function has( string $name ) : bool
    {
        return $this->getParams( )->has( $name );
    }
    
    public function remove( string $name ) : self
    {
        if( empty( $name ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->getParams( )->remove( $name );
        return $this;
    }
    
    public function get( string $name = null, $default = null)
    {
        
        if( null === $name ) {
            return $this->getParams( )->toArray( );
        }
        
        return $this->getParams( )->get( $name, $default );
    }
    
    public function toString( ) : string
    {
        $string = "";
        
        foreach( $this->getParams( ) as $name => $value ) {
            $string .= sprintf( "%s: %s\r\n", $name, $value );
        }
        
        return $string;
    }
    
    public function send( ) : self
    {
        if( headers_sent( ) ) {
            throw new HeadersAlreadySentException( );
        }
        
        foreach( $this->getParams( ) as $name => $value ) {
            header( sprintf( "%s: %s", $name, $value ) );
        }
        
        return $this;
    }
    
    public function __toString( ) 
    {
        return $this->toString( );        
    }
}
