<?php

/**
 * Thunderion Framework
 * 
 * @author Anton Iulian-Leonard
 * @email anton.iulian.leonard@gmail.com
 */

namespace Thunderion;

class Parameters implements \Iterator, \Countable, \Serializable
{
    /**
     * @var array
     */
    protected $_data = [];
    
    /**
     * Class Construct
     * 
     * @param array|null $parameters
     * @return Thunderion\Thunderion
     */
    
    public function __construct( array $parameters = null )
    {
        if( null !== $parameters ) {
            foreach( $parameters as $key => $value ) {
                $this->set( $key, $value );
            }
        }
    }
    
    /**
     * Set a parameter
     * 
     * @param string $key
     * @param mixed $value
     * @return Thunderion\Parameters
     */
    
    public function set( string $key, $value )
    {
        $this->_data[ $key ] = $value;
        
        return $this;
    }
    
    /**
     * Check if parameter exists
     * 
     * @param string $key
     * @return bool
     */
    
    public function has( string $key ) : bool
    {
        return array_key_exists( $key , $this->_data );
    }
    
    /**
     * Get value for a parameter
     * 
     * @param string $key
     * @param mixed default
     * @return mixed
     */
    
    public function get( string $key, $default = null )
    {
        return $this->has( $key ) ? $this->_data[ $key ] : $default;
    }
    
    /**
     * Remove a parameter
     * 
     * @param string $key
     * @return Thunderion\Parameter
     */
    
    public function remove( string $key )
    {
        if( $this->has( $key ) ) {
            unset( $this->_data[ $key ] );
        }
        
        return $this;
    }
    
    /**
     * Get a list of paramters keys
     * 
     * @return array
     */
    
    public function getKeys( ) : array
    {
        return array_keys( $this->_data );
    }
    
    /**
     * Return the a list of parameters
     * 
     * @return array
     */
    
    public function toArray( ) : array
    {
        return $this->_data;
    }
    
    /**
     * Return a string representation 
     * 
     * @return string
     */
    
    public function toString( ) : string
    {
        return serialize( $this->toArray( ) );
    }
    
    /**
     * Return a JSON string on success
     * 
     * @return mixed
     */
    
    public function toJson( ) : string
    {
        return json_encode( $this->toArray( ) );
    }
    
    /**
     * Magic method __get
     * 
     * @param string key
     * @return mixed
     */
    
    public function __get( string $key )
    {
        return $this->get( $key );
    }
    
    /**
     * Magic method __set
     * 
     * @param string key
     * @param mixed $value
     * @return void
     */
    
    public function __set( string $key, $value )
    {
        $this->set( $key, $value );
    }
    
    /**
     * Magic method __isset
     * 
     * @param string key
     * @return bool
     */
    
    public function __isset( string $key )
    {
        return $this->has( $key );
    }
    
    /**
     * Magic method __toString
     * 
     * @return string
     */
    
    public function __toString( ) : string
    {
        return $this->toString( );
    }
    
    /**
     * Checks if the parameter is type numeric
     * 
     * @param string $key
     * @return bool
     */
    
    public function isNumeric( string $key ) : bool
    {
        return ( $this->has( $key ) && is_numeric( $this->_data[ $key ] ) );
    }
    
    /**
     * Checks if the parameter is type boolean
     * 
     * @param string $key
     * @return bool
     */
    
    public function isBoolean( string $key ) : bool
    {
        return ( $this->has( $key ) && is_bool( $this->_data[ $key ] ) );
    }
    
    /**
     * Alias for isBoolean
     * 
     * @param string $key
     * @return bool
     */
    
    public function isBool( string $key ) : bool
    {
        return $this->isBoolean( $key );
    }
    
    /**
     * Checks if the parameter is type string
     * 
     * @param string $key
     * @return bool
     */
    
    public function isString( string $key ) : bool
    {
        return ( $this->has( $key ) && is_string( $this->_data[ $key ] ) );
    }
    
    /**
     * Checks if all of the characters in the parameter are alphanumeric
     * 
     * @param string $key
     * @return bool
     */
    
    public function isAlnum( string $key ) : bool
    {
        return $this->has( $key ) && ( is_string( $this->_data[ $key ] ) || is_numeric( $this->_data[ $key ] ) ) && ctype_alnum( $this->_data[ $key ] );
    }
    
    /**
     * Checks if all of the characters in the parameter are alphabetic
     * 
     * @param string $key
     * @return bool
     */
    
    public function isAlpha( string $key ) : bool
    {
        return $this->isString( $key ) && ctype_alpha( $this->_data[ $key ] );
    }
    
    /**
     * Converts the parameter to a integer
     * 
     * @param string $key
     * @param int $default
     * @return int
     */
    
    public function getInteger( string $key, int $default = 0 ) : int
    {
        return $this->has( $key ) && !is_object( $this->_data[ $key ] ) && !is_array( $this->_data[ $key ] ) ? (int) $this->_data[ $key ] : $default;
    }
    
    /**
     * Alias for getInteger
     * 
     * @param string $key
     * @param int $default
     * @return int
     */
    
    public function getInt( string $key, int $default = 0 ) : int
    {
        return $this->getInteger( $key, $default );
    }
    
    /**
     * Converts the parameter to a boolean
     * 
     * @param string $key
     * @param bool $default
     * @return bool
     */
    
    public function getBoolean( string $key, bool $default = false ) : bool
    {
        return $this->has( $key ) && !is_object( $this->_data[ $key ] ) && !is_array( $this->_data[ $key ] ) ? (bool) $this->_data[ $key ] : $default;
    }
    
    /**
     * Alias for getBoolean
     * 
     * @param string $key
     * @param bool $default
     * @return bool
     */
    
    public function getBool( string $key, bool $default = false ) : bool
    {
        return $this->has( $key ) && !is_object( $this->_data[ $key ] ) && !is_array( $this->_data[ $key ] ) ? (bool) $this->_data[ $key ] : $default;
    }
    
    /**
     * Converts the parameter to a float
     * 
     * @param string $key
     * @param double $default
     * @return double
     */
    
    public function getFloat( string $key, float $default = 0 ) : float
    {
        return $this->has( $key ) && !is_object( $this->_data[ $key ] ) && !is_array( $this->_data[ $key ] ) ? (float) $this->_data[ $key ] : $default;
    }
    
    /**
     * Alias for getFloat
     * 
     * @param string $key
     * @param double $default
     * @return double
     */
    
    public function getDouble( string $key, float $default = null ) : float
    {
        return $this->has( $key ) && !is_object( $this->_data[ $key ] ) && !is_array( $this->_data[ $key ] ) ? (float) $this->_data[ $key ] : $default;
    }
    
    /**
     * Alias for getFloat
     * 
     * @param string $key
     * @param double $default
     * @return double
     */
    
    public function getReal( string $key, float $default = null ) : float
    {
        return $this->has( $key ) && !is_object( $this->_data[ $key ] ) && !is_array( $this->_data[ $key ] ) ? (float) $this->_data[ $key ] : $default;
    }
    
    /**
     * Return only alphanumeric characters from parameter
     * 
     * @param string $key
     * @param string $default
     * @return string
     */
    
    public function getAlnum( string $key, string $default = '' ) : string
    {
        return $this->has( $key ) && ( is_string( $this->_data[ $key ] ) || is_numeric( $this->_data[ $key ] ) ) ? preg_replace( '/[^[:alnum:]]/', '', $this->_data[ $key ] ) : $default;
    }
    
    /**
     * Return only alphabetic characters from parameter
     * 
     * @param string $key
     * @param string $default
     * @return string
     */
    
    public function getAlpha( string $key, string $default = '' ) : string
    {
        return $this->has( $key ) && ( is_string( $this->_data[ $key ] ) || is_numeric( $this->_data[ $key ] ) ) ? preg_replace( '/[^[:alpha:]]/', '', $this->_data[ $key ] ) : $default;
    }
    
    /**
     * Return only digits from parameter
     * 
     * @param string $key
     * @param string $default
     * @return string
     */
    
    public function getDigits( string $key, string $default = '' ) : string
    {
        return $this->has( $key ) && ( is_string( $this->_data[ $key ] ) || is_numeric( $this->_data[ $key ] ) ) ? preg_replace( '/[^0-9]/', '', $this->_data[ $key ] ) : $default;
    }
    
    /**
     * Return the key of the current parameter
     * 
     * @return mixed
     */
    
    public function key( )
    {
        return key( $this->_data );
    }
    
    /**
     * Rewind the Iterator to the first parameter
     * 
     * @return mixed;
     */
    
    public function rewind( )
    {
        return reset( $this->_data );
    }
    
    /**
     * Checks if current position is valid
     * 
     * @return bool
     */
    
    public function valid( ) : bool
    {
        return ( null !== $this->key( ) );
    }
    
    /**
     * Return the current parameter
     * 
     * @return mixed
     */
    
    public function current( )
    {
        return current( $this->_data );
    }
    
    /**
     * Move forward to next parameter
     * 
     * @return mixed
     */
    
    public function next( )
    {
        return next( $this->_data );
    }
    
    /**
     * Count parameters
     * 
     * @return int
     */
    
    public function count(  ) : int
    {
        return count( $this->_data );
    }
    
    /**
     * String representation of object
     * 
     * @return string
     */
    
    public function serialize( ) : string
    {
        return $this->toString( );
    }
    
    /**
     * Constructs the object
     * 
     * @return void
     */
    
    public function unserialize( $serialized )
    {
        $this->_data = unserialize( $serialized );
    }
    
    public function isEmpty( ) : bool
    {
        return empty( $this->_data );
    }
}
