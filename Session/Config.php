<?php

namespace Thunderion\Session;

use Thunderion\Parameters;
use Thunderion\Session\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Session\Exception\InvalidConfigMethod as InvalidConfigMethodException;

class Config extends Parameters
{
    public function set( string $key, $value )
    {
        parent::set( $key, $value );
        
        if( false === ini_set( 'session.' . $key, $value ) ) {
            throw new InvalidArgumentException( sprintf( '%s is not a valid sessions-related ini setting', $key ) );
        }
        
        return $this;
    }
    
    public function get( string $key, $default = null ) 
    {
        if( $this->has( $key ) ) {
            return $this->_data[ $key ];
        }
        
        if( null !== ( $value = ini_get( 'session.' . $key ) ) ) {
            parent::set( $key, $value );
            return $value;
        }
        
        return $default;
    }
    
    public function __call( $method, $args )
    {
        $prefix = substr( $method, 0, 3 );
        $option = substr( $method, 3 );
        $key    = strtolower( preg_replace('#(?<=[a-z])([A-Z])#', '_\1', $option ) );
        $first  = array_shift( $args );

        if( 'get' === $prefix ) {
            return $this->get( $key, $first );
        } else if( 'set' === $prefix ) {
            return $this->set( $key, $first );
        } else {
            throw new InvalidConfigMethodException( );
        }
    }
    
    
}
