<?php

namespace Thunderion\Session\SaveHandler;

interface SaveHandlerInterface 
{
    public function open( $path,$name );
    public function close( );
    public function read( $id );
    public function write( $id, $data );
    public function destroy( $id );
    public function gc( $maxlifetime );
}
