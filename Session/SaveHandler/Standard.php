<?php

namespace Thunderion\Session\SaveHandler;
use Thunderion\Session\SaveHandler\SaveHandlerInterface;
use SessionHandler;

class Standard extends SessionHandler implements SaveHandlerInterface
{
    public function __construct( ) 
    {
        if ( ! extension_loaded('openssl') || !extension_loaded('mbstring') ) {
            throw new \RuntimeException( );
        }
    }
    
    private function encrypt( $data )
    {
        $iv = random_bytes( 16 );
        $key = random_bytes( 64 );
        $ciphertext = openssl_encrypt( $data, 'AES-256-CBC', mb_substr( $key, 0, 32, '8bit' ), OPENSSL_RAW_DATA, $iv );
        $hmac = hash_hmac( 'SHA256', $iv . $ciphertext, mb_substr($key, 32, null, '8bit' ), true );
        return $hmac . $iv . $key . $ciphertext ;
    }
    
    public function decrypt( $data )
    {
        $hmac       = mb_substr( $data, 0, 32, '8bit' );
        $iv         = mb_substr( $data, 32, 16, '8bit' );
        $key        = mb_substr( $data, 48, 64, '8bit' );
        $ciphertext = mb_substr( $data, 112, null, '8bit' );
        
        $hmacNew    = hash_hmac( 'SHA256', $iv . $ciphertext, mb_substr($key, 32, null, '8bit'), true );
        
        if ( !hash_equals( $hmac, $hmacNew ) ) {
            return '';
        }
        
        return openssl_decrypt( $ciphertext, 'AES-256-CBC', mb_substr( $key, 0, 32, '8bit'), OPENSSL_RAW_DATA, $iv );
    }
    
    public function open( $path, $name )
    {
        return parent::open( $path, $name );
    }
    
    public function close( ) 
    {
        return parent::close( );
    }
    
    public function read( $id )
    {
        $data = parent::read( $id );
        return empty( $data ) ? '' : $this->decrypt( $data );
    }
    
    public function write( $id, $data )
    {
        return parent::write( $id, $this->encrypt( $data ) );
    }
    
    public function destroy($id )
    {
        return parent::destroy( $id );
    }
    
    public function gc( $maxlifetime )
    {
        return parent::gc( $maxlifetime );
    }
}

