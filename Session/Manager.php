<?php

namespace Thunderion\Session;

use Thunderion\Session\Exception\AlreadyStarted as AlreadyStartedException;
use Thunderion\Session\Exception\RegisterSaveHandler as RegisterSaveHandlerException;
use Thunderion\Session\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Session\SaveHandler\SaveHandlerInterface;
use Thunderion\Session\Config as SessionConfig;

class Manager
{
    protected $_save_handler    = null;
    protected $_config          = null;
    
    public function isStarted( )
    {
        $sid = defined( 'SID' ) ? constant( 'SID' ) : false;
        
        return ( false !== $sid && $this->getId( ) || headers_sent( ) );  
    }
    
    public function setId( string $id ) : self
    {
        if( $this->isStarted( ) ) {
            throw new AlreadyStartedException( );
        }
        
        session_id( $id );
        return $this;
    }
    
    public function getId( ) : string
    {
        return session_id( );
    }
    
    public function regenerateId( bool $deleteOldSession = false ) : self
    {
        session_regenerate_id( $deleteOldSession );
        return $this;
    }
    
    public function setSaveHandler( SaveHandlerInterface $saveHandler ) : self
    {
        $this->_save_handler = $saveHandler;
        return $this;
    }
    
    public function getSaveHandler( )
    {
        return $this->_save_handler;
    }
    
    public function registerSaveHandler( SaveHandlerInterface $saveHandler ) : self
    {
        $flag = session_set_save_handler(
                array( $saveHandler, 'open' ),
                array( $saveHandler, 'close' ),
                array( $saveHandler, 'read' ),
                array( $saveHandler, 'write' ),
                array( $saveHandler, 'destroy' ),
                array( $saveHandler, 'gc' )
        );
        
        if( false === $flag ) {
            throw new RegisterSaveHandlerException( );
        }
        
        return $this;
    }
    
    public function setConfig( $config ) : self
    {
        if( is_array( $config ) ) {
            $config = new Config( $config );
        } else if( !( $config instanceof SessionConfig ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->_config = $config;
        return $this;
    }
    
    public function getConfig( ) : SessionConfig
    {
        if( null === $this->_config ) {
            $this->setConfig( new SessionConfig( ) );
        }
        
        return $this->_config;
    }
    
    public function start( ) : self
    {
        if( $this->isStarted( ) ) {
            return $this;
        }
        
        if( null !== $this->getSaveHandler( ) ) {
            $this->registerSaveHandler( $this->getSaveHandler( ) );
        }
        
        session_start( );
        return $this;
    }
    
    public function destroy( ) : self
    {
        if( !$this->isStarted( ) ) {
            return $this;
        }
        
        session_destroy( );
        
        $this->expireSessionCookie( );
        
        if( null != $this->getSaveHandler( ) ) {
            $this->getSaveHandler( )->destroy( $this->getId( ) );
        }
        
        return $this;
    }
    
    public function expireSessionCookie( ) : self
    {
        if( !(bool) $this->getConfig( )->getUseCookies( ) ) {
            return $this;
        }
        
        setCookie( $this->getConfig( )->getName( ), '', time( ) - 3600, $this->getConfig( )->getCookiePath( ), $this->getConfig( )->getCookieDomain( ), $this->getConfig( )->getCookieSecure( ), $this->getConfig( )->getCookieHttpOnly( ) );
        return $this;
    }
    
    public function setName( string $name ) : self
    {
        if( $this->isStarted( ) ) {
            throw new AlreadyStartedException( );
        }
        
        if( !preg_match('/^[a-zA-Z0-9]+$/', $name) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->getConfig( )->setName( $name );
        session_name( $name );
        return $this;
    }
    
    public function setCookieLifetime( int $ttl ) : self
    {
        if( !$this->getConfig( )->getUseCookies( ) ) {
            return $this;
        }
        
        $this->getConfig( )->setCookieLifetime( $ttl );
        
        if( $this->isStarted( ) ) {
            $this->regenerateId( );
        }
        
        return $this;
    }
    
    public function writeClose( ) : self
    {
        session_write_close();
        return $this;
    }
    
    public function get( string $key = null, $default = null )
    {
        if( null === $key ) {
            return $this->toArray( );
        }
        
        return isset( $_SESSION[ $key ] ) ? $_SESSION[ $key ] : $default;
    }
    
    public function set( string $key, $value ) : self
    {
        $_SESSION[ $key ] = $value;
        return $this;
    }
    
    public function has( string $key ) : bool
    {
        return ( is_array( $_SESSION ) && array_key_exists( $key , $_SESSION ) );
    }
    
    public function toArray( ) : array
    {
        return is_array( $_SESSION ) ? $_SESSION : array( );
    }
    
    public function __get( string $key )
    {
        return $this->get( $key );
    }
    
    public function __set( string $key, $value ) 
    {
        return $this->set( $key, $value );
    }
    
    public function __isset( string $key )
    {
        return $this->has( $key );
    }
}

