<?php

/**
 * Thunderion Framework
 * 
 * @author Anton Iulian-Leonard
 * @email anton.iulian.leonard@gmail.com
 */

namespace Thunderion;

class Autoload
{
    /**
     * @var array
     */
    protected $_namespaces      = [];
    
    /**
     * @var Thunderion\Autoload
     */
    
    protected static $_instance = null;
    
    /**
     * Class Construct
     * 
     * @param array|null $namespaces
     * @return Thunderion\Autoload
     */
    
    public function __construct( array $namespaces = null )
    {
        if( null !== $namespaces ) {
            $this->registerNamespaces( $namespaces );
        }
        
        spl_autoload_register( array( $this, 'load' ) );
    }
    
    /**
     * Normalize path
     * 
     * @param string $path
     * @return string
     */
    
    private function normalizePath( string $path ) : string
    {
        return rtrim( $path, '\/' );
    }
    
    /**
     * Register a namespace
     * 
     * @param string $namespace
     * @param string $path
     * @return Thunderion\Autoload
     */
    
    public function registerNamespace( string $namespace, string $path ) : self
    {
        $this->_namespaces[] = (object) array(
            'namespace' => $namespace,
            'path'      => $this->normalizePath( $path )
        );
        
        return $this;
    }
    
    /**
     * Register a list of namespaces
     * 
     * @param array $namespaces
     * @return Thunderion\Autoload
     */
    
    public function registerNamespaces( array $namespaces ) : self
    {
        foreach( $namespaces as $namespace => $paths ) {
            
            if( is_string( $paths ) ) {
                $this->registerNamespace( $namespace, $paths );
                continue;
            } else if( !is_array( $paths ) ) {
                continue;
            }
            
            foreach( $paths as $path ) {
                $this->registerNamespace( $namespace, $path );
            }
        }
        
        return $this;
    }
    
    /**
     * Return a list of namespaces and include paths
     * 
     * @return array
     */
    
    public function getNamespaces( ) : array
    {
        return $this->_namespaces;
    }
    
    /**
     * Remove a namespaces by array key
     * 
     * @param int $key
     * @return Thunderion\Autoload
     */
    
    public function removeNamespaceByKey( int $key ) : self 
    {
        if( isset( $this->_namespaces[ $key ] ) ) {
            unset( $this->_namespaces[ $key ] );
        }
        
        return $this;
    }
    
    /**
     * Remove a namespace
     * 
     * @param string $namespace
     * @return Thunderion\Autoload
     */
    
    public function removeNamespace( string $namespace ) : self
    {
        foreach( $this->getNamespaces( ) as $key => $o ) {
            if( $namespace === $o->namespace )  {
                $this->removeNamespaceByKey( $key );
            }
        }
        
        return $this;
    }
    
    /**
     * Remove a list of namespaces
     * 
     * @param array $namespaces
     * @return Thunderion\Autoload
     */
    
    public function removeNamespaces( array $namespaces ) : self 
    {
        foreach( $this->getNamespaces( ) as $key => $o ) {
            if( in_array( $o->namespace, $namespaces ) )  {
                $this->removeNamespaceByKey( $key );
            }
        }
        
        return $this;
    }
    
    /**
     * Include a path 
     * 
     * $param string $path
     * @return bool
     */
    
    private function loadPath( string $path ) : bool
    {
        return file_exists( $path ) ? include_once $path : false;
    }
    
    /*
     * Check if a file for a class exists and load it
     * 
     * @param string $class
     * @return bool
     */
    
    public function load( string $class ) : bool
    {
        foreach( $this->getNamespaces( ) as $o ) {
            
            if( 0 !== strpos( $class, $o->namespace ) ) {
                continue;
            }
            
            if( $this->loadPath( $o->path . str_replace( '\\', DIRECTORY_SEPARATOR, substr( $class, strlen( $o->namespace ) ) ) . '.php' ) ) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * get Object Instance
     * 
     * @return Thunderion\Autoload
     */
    
    public static function getInstance( ) : self
    {
        if( null === self::$_instance ) {
            self::$_instance = new self( );
        }
        
        return self::$_instance;
    }
    
    /**
     * Primary register method
     * 
     * @param array|null $namespaces
     * @return Thunderion\Autoload
     */
    
    public static function register( array $namespaces = null ) : self
    {
        $object = self::getInstance( );
        
        if( null !== $namespaces ) {
            $object->registerNamespaces( $namespaces );
        }
        
        return $object;
    }
    
    
}

