<?php

namespace Thunderion\Database\Mysql;

use Thunderion\Config\Config;

class Handler
{
    protected $_connection                  = null;
    
    protected $_table_prefix                = '';
    
    protected $_last_inserted_id            = false;
    
    protected $_rows_found                  = 0;
    
    protected static $_instances            = array( );
    
    
    public static function getConnection( Config $config )
    {
        $key = md5( $config->toString( ) );
        
        if( !isset( self::$_instances[ $key ] ) ) {
            self::$_instances[ $key ] = new self( $config );
        }
        
        return self::$_instances[ $key ];
    }


    public function __construct( Config $config = null ) 
    {
        if( null !== $config ) {
            $this->connect( $config->host, $config->username, $config->password, $config->dbname );
            
            if( $config->tablePrefix ) {
                $this->setTablePrefix( $config->tablePrefix );
            }
        }
    }
    
    public function connect( $host, $username, $password, $dbname )
    {
        $this->_connection = @mysqli_connect($host, $username, $password, $dbname);
        if( !$this->_connection || mysqli_errno( $this->_connection ) ) {
            throw new \Exception( mysqli_connect_error( ), mysqli_connect_errno( ) );
        }
        mysqli_set_charset( $this->_connection , 'utf8' );
    }
    
    public function setTablePrefix( $prefix )
    {
        $this->_table_prefix = trim( (string) $prefix );
        return $this;
    }
    
    public function getTableName( $name )
    {
        return $this->_table_prefix . $name;
    }
    
    public function __get( $name )
    {
        return $this->getTableName( $name );
    }
    
    public function escape( $string )
    {
        $string = trim( $string );
        
        if( @get_magic_quotes_gpc( ) ) {
            $string  =  stripslashes( $string );
        }
        
        return mysqli_real_escape_string( $this->_connection, $string );
    }
    
    private function _escape( &$string )
    {
        if( !is_float( $string ) ) {
            $string = $this->escape( $string );
        }
        return $string;
    }
    
    public function query( $sql )
    {
        if( !$this->_connection || !( $query = mysqli_query( $this->_connection, $sql ) ) ) {
            return false;
        }
        $this->_last_inserted_id = mysqli_insert_id( $this->_connection );
        
        if( is_object( $query ) ) {
            $this->_rows_found = mysqli_num_rows( $query );
        }
        
        return $query;
    }
    
    public function multiQuery( $sql ) 
    {
        if( !$this->_connection || !( $query = mysqli_multi_query( $this->_connection, $sql ) ) ) {
            return false;
        }
        
        $this->_last_inserted_id = mysqli_insert_id( $this->_connection );
         
        if( is_object( $query ) ) {
            $this->_rows_found = mysqli_num_rows( $query );
        }
        
        return $query;
    }
    
    public function fetch( $sql, $type = MYSQLI_ASSOC )
    {
        if( !( $query = $this->query( $sql ) ) ) {
            return array();
        }
        
        $array = array();
        
        while( ($row = mysqli_fetch_array( $query, $type )) ) {
            $array[] = $type === MYSQLI_ASSOC ? (object) $row : $row;
        }
        
        mysqli_free_result( $query );
        
        return $array;
    }
    
    public function fetchRow( $sql, $type = MYSQLI_ASSOC, $row_num = 0 )
    {
        if( !( $query = $this->query( $sql ) ) ) {
            return array();
        }
        
        $count = 0;
        
        while( ( $row = mysqli_fetch_array( $query, $type ) ) ) {
            if( $count === $row_num ) {
                return $type === MYSQLI_ASSOC ? (object) $row : $row;
            }
            $count++;
        }
    }
    
    public function fetchVar( $sql, $default = null, $row_num = 0, $col_num = 0 )
    {
        if( !( $query = $this->query( $sql ) ) ) {
            return array();
        }
        
        $count = 0;
        
        while( $row = mysqli_fetch_array( $query, MYSQLI_NUM ) ) {
            if( $count !== $row_num ) {
                $count++;
                continue;
            }
            return isset( $row[ $col_num ] ) ?  $row[ $col_num ] : $default;
        }
    }
    
    public function fetchCol( $sql, $col_num = 0 )
    {
        if( !( $query = $this->query( $sql ) ) ) {
            return array();
        }
        
        $array = array();
        
        while( $row = mysqli_fetch_array( $query, MYSQLI_NUM ) ) {
            if( !isset( $row[$col_num] ) ) {
                return array();
            }
            
            $array[] = $row[ $col_num ];
        }
        
        return $array;
    }
    
    public function prepare( )
    {
        $args = func_get_args(  );
        $sql = array_shift( $args );
        $sql = preg_replace( '|(?<!%)%f|' , '%F', $sql );
        $sql = preg_replace( '|(?<!%)%s|', "'%s'", $sql);
        
        array_walk( $args, array( $this, '_escape' ) );
        
        return @vsprintf( $sql, $args );
    }
    
    public function getLastInsertedID( )
    {
        return $this->_last_inserted_id;
    }
    
    public function getRowsFound( )
    {
        return $this->_rows_found;
    }
    
    public function getLastError( )
    {
        return mysqli_error( $this->_connection );
    }
}
