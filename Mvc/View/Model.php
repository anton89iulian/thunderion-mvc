<?php

namespace Thunderion\Mvc\View;

use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Mvc\View\ModelInterface;
use Thunderion\Http\Request as HttpRequest;
use Thunderion\Parameters;
use Thunderion\i18n\Translator\Translator;
use Thunderion\i18n\Translator\TranslatorInterface;

class Model implements ModelInterface
{
    protected $_template_path = '/';
    protected $_template_name = '';
    protected $_http_request = null;
    protected $_parameters    = null;
    protected $_translator    = null;
    
    public function __construct( $name = null, array $params = null ) 
    {
        if( null !== $name ) {
            $this->setTemplateName( $name );
        }
        
        if( null !== $params ) {
            $this->setParams( $params );
        }
    }
    
    public function setHttpRequest( HttpRequest $request )
    {
        $this->_http_request = $request;
        return $this;
    }
    
    public function getHttpRequest( ) : HttpRequest
    {
        if( null === $this->_http_request ) {
            $this->setHttpRequest( new HttpRequest( ) );
        }
        
        return $this->_http_request;
    }
    
    public function setTranslator( TranslatorInterface $translator )
    {
        $this->_translator = $translator;
        return $this;
    }
    
    public function getTranslator( ) : TranslatorInterface
    {
        if( null === $this->_translator ) {
            $this->setTranslator( new Translator( ) );
        }
        
        return $this->_translator;
    }
    
    public function setTemplatePath( string $path )
    {
        $this->_template_path = rtrim( $path, '\/' );
        return $this;
    }
    
    public function getTemplatePath( ) : string
    {
        return $this->_template_path;
    }
    
    public function setTemplateName( string $name )
    {
        $this->_template_name = trim( $name, '\/' );
        return $this;
    }
    
    public function getTemplateName( ) : string
    {
        return $this->_template_name;
    }
    
    public function requiresTemplate( ): bool 
    {
        return true;
    }
    
    public function setParams( $params )
    {
        if( is_array( $params ) ) {
            $params = new Parameters( $params );
        } else if( !( $params instanceof Parameters ) ) {
            throw InvalidArgumentException;
        }

        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams( ) : Parameters
    {
        if( null === $this->_parameters ) {
            $this->setParams( new Parameters( ) );
        }
        
        return $this->_parameters;
    }
    
    public function set( string $key, $value )
    {
        $this->getParams( )->set( $key, $value );
        return $this;
    }
    
    public function get( string $key, $default = null )
    {
        return $this->getParams( )->get( $key, $default );
    }
    
    public function __get( string $key )
    {
        return $this->get( $key );
    }
    
    public function __set( string $key, $value )
    {
        return $this->set( $key, $value );
    }
    
    public function __isset( string $key ) : bool
    {
        return $this->getParams( )->has( $key );
    }
    
    public function assign( array $values )
    {
        foreach( $values as $key => $value ) {
            $this->getParams( )->set( $key, $value );
        }
        
        return $this;
    }
    
    public function render( string $name = null ) : string
    {
        $path = $this->getTemplatePath( ) . '/' . ( null === $name ? $this->getTemplateName( ) : trim( $name, '\/' ) ) . '.php';
        
        if( !file_exists( $path ) ) {
            return '';
        }
        
        ob_start( );
        include $path;
        return ob_get_clean( );
    }
    
    public function toString( string $name = null ) : string
    {
        return $this->render( $name );
    }
    
    public function __toString( ) : string
    {
        return $this->render( );
    }
    
    public function _( string $message, string $locale = null )
    {
        return $this->getTranslator( )->translate( $message, $locale );
    }
    
    public function _e( string $message, string $locale = null )
    {
        echo $this->getTranslator( )->translate( $message, $locale );
        return $this;
    }
    
    protected function formatUrl( string $url, bool $show_language = true, string $language = '' )
    {
        if( filter_var( $url, FILTER_VALIDATE_URL ) !== false ) {
            return $url;
        }
        
        $string = $this->getHttpRequest()->getBaseUrl( );
        
        if( $this->getTranslator()->isActive( ) && $show_language ) {
            
            if( empty( $language )  ) {
                $language = $this->getTranslator( )->getLocale( );
            }
            
            if( $language != $this->getTranslator( )->getDefaultLanguage( ) ) {
                $string .= '/' . $language;
            }
        }
        
        $string .= '/' . ( trim( $url, '\/' ) );
        
        return $string;
    }


    protected function enqueueScript( string $src, bool $async = false )
    {
        echo '<script type="text/javascript" src="' . $this->formatUrl( $src, false ) . '"' . ( $async ? ' async' : '' ) . '></script>';
        return $this;
    }
    
    protected function enqueueStyle( string $href )
    {
        echo '<link rel="stylesheet" type="text/css" href="' . $this->formatUrl( $href, false ) . '"/>';
        return $this;
    }
    
    protected function enqueueScripts( array $files ) 
    {
        foreach( $files as $src ) {
            $this->enqueueScript( $src );
        }
        
        return $this;
    }
    
    protected function enqueueStyles( array $files )
    {
        foreach( $files as $href ) {
            $this->enqueueStyle( $href );
        }
        
        return $this;
    }
    
    protected function js( $src ) 
    {
        if( is_array( $src ) ) {
           return $this->enqueueScripts( $src );
        } else if( !is_string( $src ) ) {
            throw new InvalidArgumentException( );
        }
        
        return $this->enqueueScript( $src );
    }
    
    protected function css( $href )
    {
        if( is_array( $href ) ) {
            return $this->enqueueStyles( $href );
        } else if( !is_string( $href ) ) {
            throw new InvalidArgumentException( );
        }
        
        return $this->enqueueStyle( $href );
    }
    
    protected function buildJsString( array $array ) : string
    {
        $js = '';
        
        foreach( $array as $key => $value )
        {
            if( is_numeric( $key[0] ) ) {
                continue;
            }
            $js .= ', ' . $key . " = " . json_encode( $value ) ;
        }
        
        return $js !== '' ? 'var ' . ltrim( $js, ', ' ) . ';' : '';
    }
    
    protected function buildCssString( string $selector, array $properties )
    {
        
    }
    
    protected function addScript( array $array )
    {
        echo '<script type="text/javascript">' . $this->buildJsString( $array ) . '</script>';
        return $this;
    }
    
    protected function addStyle( array $array )
    {
        
    }

}
