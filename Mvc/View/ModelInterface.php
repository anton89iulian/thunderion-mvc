<?php

namespace Thunderion\Mvc\View;

use Thunderion\Http\Request as HttpRequest;
use Thunderion\Parameters;
use Thunderion\i18n\Translator\TranslatorInterface;

interface ModelInterface 
{
    public function setHttpRequest( HttpRequest $request );
    
    public function getHttpRequest( ) : HttpRequest;
    
    public function setTranslator( TranslatorInterface $translator );
    
    public function getTranslator( ) : TranslatorInterface;
    
    public function setParams( $params );
    
    public function getParams( ) : Parameters;
    
    public function setTemplatePath( string $path );
    
    public function getTemplatePath( ) : string;
    
    public function requiresTemplate( ) : bool;
    
    public function set( string $key, $value );
    
    public function get( string $key, $default = null );
    
    public function toString( string $name = null ) : string;
        
    public function __toString( ) : string;
}
