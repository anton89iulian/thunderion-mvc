<?php

namespace Thunderion\Mvc\Controller;

use Thunderion\Mvc\Controller\ControllerInterface;
use Thunderion\Http\Request as HttpRequest;
use Thunderion\Http\Response as HttpResponse;
use Thunderion\Mvc\Service\Manager as ServiceManager;
use Thunderion\Session\Manager as SessionManager;
use Thunderion\Mvc\Controller\Dispatcher as ControllerDispatcher;
use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Parameters;
use Thunderion\i18n\Translator\Translator;
use Thunderion\i18n\Translator\TranslatorInterface;

abstract class Controller implements ControllerInterface
{
    protected $_http_request    = null;
    protected $_http_response   = null;
    protected $_service_manager = null;
    protected $_parameters      = null;
    protected $_dispatcher      = null;
    protected $_session_manager = null;
    protected $_translator      = null;
    
    public function init( )
    {
        
    }
    
    public function setHttpRequest( HttpRequest $request )
    {
        $this->_http_request = $request;
        return $this;
    }
    
    public function getHttpRequest( ): HttpRequest 
    {
        if( null === $this->_http_request ) {
            $this->setRequest( new HttpRequest( ) );
        }
        
        return $this->_http_request;
    }
    
    public function setHttpResponse( HttpResponse $response )
    {
        $this->_http_response = $response;
        return $this;
    }
    
    public function getHttpResponse( ) : HttpResponse
    {
        if( null === $this->_http_response ) {
            $this->setHttpResponse( new HttpResponse( ) );
        }
        
        return $this->_http_response;
    }
    
    public function setServiceManager( ServiceManager $manager ) 
    {
        $this->_service_manager = $manager;
        return $this;
    }
    
    public function getServiceManager(): ServiceManager 
    {
        if( null === $this->_service_manager ) {
            $this->setServiceManager( new ServiceManager( ) );
        }
        
        return $this->_service_manager;
    }
    
    public function setSessionManager( SessionManager $manager )
    {
        $this->_session_manager = $manager;
        return $this;
    }
    
    public function getSessionManager( ): SessionManager
    {
        if( null === $this->_session_manager ) {
            $this->setSessionManager( new SessionManager( ) );
        }
        
        return $this->_session_manager;
    }
    
    
    public function setTranslator( TranslatorInterface $translator )
    {
        $this->_translator = $translator;
        return $this;
    }
    
    public function getTranslator( ) : TranslatorInterface
    {
        if( null === $this->_translator ) {
            $this->setTranslator( new Translator( ) );
        }
        
        return $this->_translator;
    }
    
    public function setParams( $params )
    {
        if( is_array( $params ) ) {
            $params = new Parameters( $params );
        } else if( !( $params instanceof Parameters ) ) {
            throw InvalidArgumentException;
        }

        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams( ) : Parameters
    {
        if( null === $this->_parameters ) {
            $this->setParams( new Parameters( ) );
        }
        
        return $this->_parameters;
    }
    
    public function setDispatcher( ControllerDispatcher $dispatcher )
    {
        $this->_dispatcher = $dispatcher;
        return $this;
    }
    
    public function getDispatcher(  ): ControllerDispatcher
    {
        return $this->_dispatcher;
    }
    
    public function getParam( string $key, $default = null )
    {
        return $this->getParams( )->get( $key, $default );
    }
    
    public function get( string $key, $default = null )
    {
        return $this->getParam( $key, $default );
    }
    
    public function forward( string $action, $controller = null, $params = null ) : HttpResponse
    {
        if( null === $controller ) {
            $controller = $this;
        }
        
        return $this->getDispatcher( )->forward( $action , $controller, $params );
    }
    
    public function _( string $message, string $locale = null )
    {
        return $this->getTranslator( )->translate( $message, $locale );
    }
    
    public function _e( string $message, string $locale = null )
    {
        echo $this->getTranslator( )->translate( $message, $locale );
        return $this;
    }
}
