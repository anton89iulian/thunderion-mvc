<?php

namespace Thunderion\Mvc\Controller;

use Thunderion\Http\Request as HttpRequest;
use Thunderion\Http\Response as HttpResponse;
use Thunderion\Mvc\Service\Manager as ServiceManager;
use Thunderion\i18n\Translator\TranslatorInterface;
use Thunderion\Parameters;


interface ControllerInterface 
{
    public function setHttpRequest( HttpRequest $request );
    
    public function getHttpRequest( ) : HttpRequest;
    
    public function setHttpResponse( HttpResponse $response );
    
    public function getHttpResponse( ) : HttpResponse;
    
    public function setServiceManager( ServiceManager $manager );
    
    public function getServiceManager( ) : ServiceManager;
    
    public function setTranslator( TranslatorInterface $translator );
    
    public function getTranslator( ) : TranslatorInterface;
    
    public function setParams( $params );
    
    public function getParams( ): Parameters;
    
    public function init( );
}
