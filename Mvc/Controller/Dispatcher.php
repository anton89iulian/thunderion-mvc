<?php

namespace Thunderion\Mvc\Controller;

use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Mvc\Exception\ControllerNotFound as ControllerNotFoundException;
use Thunderion\Mvc\Exception\ActionNotFound as ActionNotFoundException;
use Thunderion\Mvc\Exception\InvalidActionResponse as InvalidActionResponseException;
use Thunderion\Mvc\Controller\ControllerInterface;
use Thunderion\Http\Request as HttpRequest;
use Thunderion\Http\Response as HttpResponse;
use Thunderion\Mvc\Service\Manager as ServiceManager;
use Thunderion\Mvc\View\ModelInterface as ViewModelInterface;
use Thunderion\Session\Manager as SessionManger;
use Thunderion\i18n\Translator\Translator;
use Thunderion\i18n\Translator\TranslatorInterface;

class Dispatcher 
{
    protected $_http_response           = null;
    protected $_http_request            = null;
    protected $_service_manager         = null;
    protected $_controller_namespace    = '\\';
    protected $_view_path               = '/';
    protected $_session_manager        = null;
    protected $_translator             = null;
    
    public function setControllerNamespace( string $namespace ) : self
    {
        $this->_controller_namespace = $namespace;
        return $this;
    }
    
    public function getControllerNamespace(  ) : string
    {
        return $this->_controller_namespace;
    }
    
     public function setViewPath( string $path ) : self
    {
        $this->_view_path = rtrim( $path, '\/' );
        return $this;
    }
    
    public function getViewPath( ) : string
    {
        return $this->_view_path;
    }
    
    public function setHttpRequest( HttpRequest $request ) : self
    {
        $this->_http_request = $request;
        return $this;
    }
    
    public function getHttpRequest( ): HttpRequest 
    {
        if( null === $this->_http_request ) {
            $this->setRequest( new HttpRequest( ) );
        }
        
        return $this->_http_request;
    }
    
    public function setHttpResponse( HttpResponse $response ) : self
    {
        $this->_http_response = $response;
        return $this;
    }
    
    public function getHttpResponse( ) : HttpResponse
    {
        if( null === $this->_http_response ) {
            $this->setHttpResponse( new HttpResponse( ) );
        }
        
        return $this->_http_response;
    }
    
    public function setServiceManager( ServiceManager $manager ) : self
    {
        $this->_service_manager = $manager;
        return $this;
    }
    
    public function getServiceManager(): ServiceManager 
    {
        if( null === $this->_service_manager ) {
            $this->setServiceManager( new ServiceManager( ) );
        }
        
        return $this->_service_manager;
    }
    
    public function setSessionManager( SessionManger $manager )
    {
        $this->_session_manager = $manager;
        return $this;
    }
    
    public function getSessionManager( ) : SessionManger
    {
        if( null === $this->_session_manager ) {
            $this->setSessionManager( new SessionManger( ) );
        }
        
        return $this->_session_manager;
    }
    
    public function setTranslator( TranslatorInterface $translator )
    {
        $this->_translator = $translator;
        return $this;
    }
    
    public function getTranslator( ) : TranslatorInterface
    {
        if( null === $this->_translator ) {
            $this->setTranslator( new Translator( ) );
        }
        
        return $this->_translator;
    }
    
    protected function formatControllerName( string $controller ) : string
    {
        return str_replace( ' ', '', ucwords( str_replace( array( '.', '-', '_' ), ' ', $controller ) ) );
    }
    
    protected function formatActionName( string $action ) : string
    {
        return lcfirst( str_replace( ' ', '', ucwords( str_replace( array( '.', '-', '_' ), ' ', $action ) ) ) ) . 'Action' ;
    }
    
    protected function controllerToClass( string $controller ) : string
    {
        return $this->getControllerNamespace( ) . '\\' . $this->formatControllerName( $controller );
    }
    
    public function forward( string $action, $controller, $params = null )
    {
        $action = $this->formatActionName( $action );

        if( is_string( $controller ) ) {
            $class = $this->controllerToClass( $controller );
            if( !class_exists( $class ) || !is_subclass_of( $class, '\Thunderion\Mvc\Controller\ControllerInterface' ) ) {
                throw new ControllerNotFoundException( ); 
            }

            $controller = new $class( );
        } else if( !( $controller instanceof ControllerInterface ) ) {
            throw new InvalidArgumentException( );
        }   
        
        $controller->setHttpRequest( $this->getHttpRequest( ) )
                   ->setHttpResponse( $this->getHttpResponse( ) )
                   ->setServiceManager( $this->getServiceManager( ) )
                   ->setSessionManager( $this->getSessionManager( ) )
                   ->setTranslator( $this->getTranslator( ) )
                   ->setDispatcher( $this );
        
        if( null !== $params ) {
            $controller->setParams( $params );
        }

        if( !method_exists( $controller , $action ) ) {
            throw new ActionNotFoundException(  );
        }
        
        $result = $controller->init( );
        
        if( null === $result ) {
            $result = call_user_func( array( $controller, $action ) );
        }
        
        if( ( $result instanceof ViewModelInterface ) ) {
            $result->setHttpRequest( $controller->getHttpRequest( ) );
            $result->setTranslator( $controller->getTranslator( ) );
            
            if( $result->requiresTemplate( ) ) {
                $result->setTemplatePath( $this->getViewPath( ) );
            }
            
        } else if( ( $result instanceof HttpResponse ) ) {
            $result = $result->getBody( );
        } else if( !is_string( $result ) && null !== $result ) {
            throw new InvalidActionResponseException( );
        }
        
        return $controller->getHttpResponse( )->setBody( (string) $result );  
    }
}
