<?php

namespace Thunderion\Mvc;

use Thunderion\Config\Config;
use Thunderion\Config\Factory as ConfigFactory;
use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Http\Response as HttpResponse;
use Thunderion\Http\Request as HttpRequest;
use Thunderion\Mvc\Router\RouteStack as Router;
use Thunderion\Mvc\Controller\Dispatcher as ControllerDispatcher;
use Thunderion\Mvc\Exception\RouteMatchNotFound as RouteMatchNotFoundException;
use Thunderion\Mvc\Exception\InvalidConfigParameters as InvalidConfigParametersException;
use Thunderion\Mvc\Service\Manager as ServiceManager;
use Thunderion\Mvc\Router\RouteStackInterface;
use Thunderion\Session\Manager as SessionManager;
use Thunderion\i18n\Translator\Translator;
use Thunderion\i18n\Translator\TranslatorInterface;
use Thunderion\Autoload;

class Application 
{
    protected $_config                  = null;
    protected $_http_response           = null;
    protected $_http_request            = null;
    protected $_router                  = null;
    protected $_controller_displatcher  = null;
    protected $_base_directory          = null;
    protected $_session_manager         = null;
    protected $_translator              = null;
    
    public function __construct( ) 
    {
        
    }
    
    
    public function setBaseDirectory( string $path ) : self
    {
        $this->_base_directory = rtrim( $path, '\/' );
        return $this;
    }
    
    public function getBaseDirectory( )
    {
        if( null === $this->_base_directory ) {
            $this->setBaseDirectory( $this->getConfig( )->get( 'applicationBaseDirectory', getcwd( ) . '/../application' ) );
        }
        
        return $this->_base_directory;
    }
    
    public function setConfig( $config ) : self
    {
        if( is_string( $config ) ) {
            $config = ConfigFactory::fromFile( $config );
        } else if( is_array( $config ) ) {
            $config = new Config( $config );
        } else if( !( $config instanceof Config ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->_config = $config;
        return $this;
    }
    
    protected function getConfig( ) : Config
    {
        if( null === $this->_config ) {
            $this->setConfig( new Config( ) );
        }
        
        return $this->_config;
    }
    
    public function setHttpResponse( HttpResponse $response ) : self
    {
        $this->_http_response = $response;
        return $this;
    }
    
    protected function getHttpResponse( ) : HttpResponse
    {
        if( null === $this->_http_response ) {
            $this->setHttpResponse( new HttpResponse( ) );
        }
        
        return $this->_http_response;
    }
    
    public function setHttpRequest( HttpRequest $request ) : self
    {
        $this->_http_request = $request;
        return $this;
    }
    
    protected function getHttpRequest( ) : HttpRequest 
    {
        if( null === $this->_http_request ) {
            $this->setHttpRequest( HttpRequest::fromPlatform( ) );
        }
        
        return $this->_http_request;
    }
    
    public function setRouter( RouteStackInterface $router ) : self
    {
        $this->_router = $router;
        return $this;
    }
    
    protected function getRouter( ): RouteStackInterface
    {
        if( null == $this->_router ) {
            $this->setRouter( new Router( ) );
        }
        
        return $this->_router;
    }
    
    public function setControllerDispatcher( ControllerDispatcher $dispatcher ) : self
    {
        $this->_controller_displatcher = $dispatcher;
        return $this;
    }
    
    public function getControllerDispatcher( ) : ControllerDispatcher
    {
        if( null === $this->_controller_displatcher ) {
            $this->setControllerDispatcher( new ControllerDispatcher( ) );
        }
        
        return $this->_controller_displatcher;
    }
    
    public function setSessionManager( SessionManager $manager ) : self
    {
        $this->_session_manager = $manager;
        return $this;
    }
    
    public function getSessionManager( ) : SessionManager
    {
        if( null === $this->_session_manager ) {
            $this->setSessionManager( new SessionManager( ) );
        }
        
        return $this->_session_manager;
    }
    
    public function setTranslator( TranslatorInterface $translator ) : self
    {
        $this->_translator = $translator;
        return $this;
    }
    
    public function getTranslator(  ) : TranslatorInterface
    {
        if( null === $this->_translator ) {
            $this->setTranslator( new Translator( ) );
        }
        
        return $this->_translator;
    }
    
    private function prepareSessionManager( ) : SessionManager
    {
        $saveHandler = trim( $this->getConfig()->get('sessionSaveHandler') );
        
        if ( $this->getConfig()->has('session') ) {
            $this->getSessionManager()->setConfig( $this->getConfig()->get('session')->toArray( ) );
        }
        
        
        if ( !empty( $saveHandler ) && class_exists( $saveHandler ) && is_subclass_of( $saveHandler, 'Thunderion\Session\SaveHandler\SaveHandlerInterface') ) {
            $this->getSessionManager( )->setSaveHandler( new $saveHandler ( ) );
        }
        
        if( (bool) $this->getSessionManager( )->getConfig( )->getAutoStart( ) || (bool) $this->getConfig( )->get( 'sessionAutoStart' ) ) {
            $this->getSessionManager( )->start( );
        }
        
        return $this->getSessionManager( );
    }
    
    private function prepareRouter( ) : RouteStackInterface
    {   
        $languagePattern = '';
        
        if ( ( $this->getConfig( )->multiLanguage instanceof Config )  && $this->getConfig( )->multiLanguage->getBool( 'enabled', false ) ) {
            
            if( ( $this->getConfig( )->multiLanguage->languages instanceof Config ) && !empty( $this->getConfig( )->multiLanguage->languages ) ) {
                $languagePattern = '/(?<language_code>(' . implode( '|', array_keys( $this->getConfig( )->multiLanguage->languages->toArray( ) ) ) . '))';
            } else {
                $languagePattern = $this->getConfig( )->multiLanguage->pattern;
            }
        }
        
        if ( !$this->getRouter( )->isReadOnly( $this->getConfig( )->getBool( 'routerReadOnly', false ) ) ) {

            $this->getRouter()->addRoute( 'controller-action', array(
                'pattern' => '(?J)/$|^/(?<controller>[a-zA-Z0-9-_]+)$|^/(?<controller>[a-zA-Z0-9-_]+)/(?<action>[a-zA-Z0-9-_]+)',
            ), 0);
            
            if( !empty( $languagePattern ) ) {
                $this->getRouter()->addRoute( 'controller-action-multilanguage', array(
                    'pattern' => '(?J)' . $languagePattern . '$|^' . $languagePattern . '/(?<controller>[a-zA-Z0-9-_]+)$|^' . $languagePattern . '/(?<controller>[a-zA-Z0-9-_]+)/(?<action>[a-zA-Z0-9-_]+)',
                ), 0);
            }
        }
        
        if ( $this->getConfig( )->has('routes') ) {
            $routes = $this->getConfig( )->routes->toArray( );
            if ( !is_array( $routes ) ) {
                throw new InvalidConfigParametersException( 'Routes must be defined as a array' );
            }
            
            foreach( $routes as $name => $route ) {
                if( !empty( $languagePattern ) && ( !isset( $route['languagePattern'] ) || empty( $route['languagePattern'] ) ) ) {
                    $route['languagePattern'] = $languagePattern;
                }
                
                $this->getRouter( )->addRoute( $name, $route );
            }
        }
        
        return $this->getRouter( );
    }
    
    private function prepareConfig( ) : Config
    {
        $this->getConfig( )->mergeWith( array(
                'namespaces'    => array(
                    'controller'    => array(
                        'namespace' => 'Application\Controller',
                        'path' => $this->getBaseDirectory( ) . '/controllers'
                    ),
                    'model'        => array(
                        'namespace' => 'Application\Model',
                        'path'      => $this->getBaseDirectory( ) . '/models'
                    ),
                    'service'     => array(
                        'namespace' => 'Application\Service',
                        'path'      => $this->getBaseDirectory( ) . '/services'
                    )
                ),
                'multiLanguage' => array(
                    'enabled' => false,
                    'locale'  => 'en',
                    'languages' => array( ),
                    'pattern' => '/(?<language_code>[a-zA-Z]{2})', 
                    'translationsBaseDir' => $this->getBaseDirectory( ) . '/languages',
                    'fileExtension' => 'mo'
                ),
                'routes' => array( ),
                'defaultController' => 'index',
                'defaultAction' => 'index',
                'viewTemplatePath' => $this->getBaseDirectory( ) . '/views',
                'routerReadOnly' => false
        ) );

        foreach ( $this->getConfig( )->namespaces as $key => $ns ) {

            if ( !isset( $ns->path ) || !isset( $ns->namespace ) ) {
                throw new InvalidConfigParametersException('Invalid namespaces parameters');
            }

            Autoload::getInstance()->registerNamespace($ns->namespace, $ns->path);
        }
        
        return $this->getConfig( );
    }
    
    public function run( )
    { 
        try {
            
            $this->prepareConfig( );
            $this->prepareRouter( );
            $this->prepareSessionManager( );
           
            if( false === ( $match = $this->getRouter( )->match( $this->getHttpRequest( ) ) ) ) {
                throw new RouteMatchNotFoundException( );
            }
   

            if( !$match->getController( ) ) {
                $match->setController( $this->getConfig( )->get( 'defaultController', 'index' ) );
            }

            if( !$match->getAction( ) ) {
                $match->setAction( $this->getConfig( )->get( 'defaultAction', 'index' ) );
            }
            
            if ( ( $this->getConfig( )->multiLanguage instanceof Config )  && $this->getConfig( )->multiLanguage->getBool( 'enabled', false ) ) {
                $language_code = $match->getParams( )->has( 'language_code' ) ? $match->getParams( )->get( 'language_code', '' ) :  $this->getConfig( )->multiLanguage->locale;
                
                if( ( $this->getConfig( )->multiLanguage->languages instanceof Config ) && !empty( $this->getConfig( )->multiLanguage->languages ) ) {
                    $languages = $this->getConfig( )->multiLanguage->languages->toArray( );
                    $locale = isset( $languages[ $language_code ] ) ? $languages[ $language_code ] : $language_code;
                } else {
                    $locale = $language_code;
                }
                    
                $this->getTranslator( )->setBaseDir( $this->getConfig( )->multiLanguage->translationsBaseDir )
                                       ->setFileType( $this->getConfig( )->multiLanguage->fileExtension )
                                       ->setLocale( $locale )
                                       ->setDefaultLanguage( $this->getConfig( )->multiLanguage->locale )
                                       ->isActive( true );
            }
            
            $serviceManager = new ServiceManager( );

            $serviceManager->setNamespace( $this->getConfig( )->namespaces->service->namespace );

            $response = $this->getControllerDispatcher( )->setHttpRequest( $this->getHttpRequest( ) )
                                                         ->setHttpResponse( $this->getHttpResponse( ) )
                                                         ->setServiceManager( $serviceManager )
                                                         ->setSessionManager( $this->getSessionManager( ) )
                                                         ->setControllerNamespace( $this->getConfig( )->namespaces->controller->namespace )
                                                         ->setViewPath( $this->getConfig( )->viewTemplatePath )
                                                         ->setTranslator( $this->getTranslator( ) )
                                                         ->forward( $match->getAction( ) , $match->getController( ), $match->getParams( ) );

            $response->dispatch( );
        } catch( \Throwable $e ) {
            echo '<pre>';
            print_r( $e );
            echo '</pre>';
            return false;
        }
        return $this;
    }
    
}
