<?php

namespace Thunderion\Mvc\Service;

use Thunderion\Service\Manager as ServiceManager;
use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Service\Service;

class Manager extends ServiceManager
{
    public function validate( $service ) 
    {
        if( $service instanceof Service ) {
            return null;
        }
        
        throw new InvalidArgumentException( );
    }
}
