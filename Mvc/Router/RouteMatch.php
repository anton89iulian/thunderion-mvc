<?php

namespace Thunderion\Mvc\Router;

use Thunderion\Parameters;
use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;

class RouteMatch 
{
    protected $_route_name;
    protected $_parameters;
   
    public function __construct( string $route_name = null, $params = null ) 
    {
        if( null !== $route_name ) {
            $this->setRouteName( $route_name );
        }
        
        if( null !== $params ) {
            $this->setParams( $params );
        }
    }

    public function setRouteName( string $name ) : self
    {
        $this->_route_name = $name;
        return $this;
    }

    public function getRouteName( ): string
    {
        return $this->_route_name;
    }

    public function setParams( $params ) : self
    {
        if( is_array( $params ) ) {
            $params = new Parameters( $params );
        } else if( !( $params instanceof Parameters ) ) {
            throw InvalidArgumentException;
        }

        $this->_parameters = $params;
        return $this;
    }

    public function getParams(  ) : Parameters
    {
        if( null === $this->_parameters ) {
            $this->setParams( new Parameters( ) );
        }

        return $this->_parameters;
    }

    public function setController( string $controller ) : self
    {
        $this->getParams( )->set( 'controller', $controller );
        return $this;
    }

    public function getController( string $default = null ) : string
    {
        return (string) $this->getParams( )->get( 'controller', $default );
    }

    public function setAction( string $action ) : self
    {
        $this->getParams( )->set( 'action', $action );
        return $this;
    }

    public function getAction( string $default = null ) : string
    {
        return (string) $this->getParams( )->get( 'action', $default );
    }
}
