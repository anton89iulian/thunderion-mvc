<?php

namespace Thunderion\Mvc\Router;

use Thunderion\Mvc\Router\RouteInterface;
use Thunderion\Parameters;
use Thunderion\Http\Request as HttpRequest;
use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;

class Route implements RouteInterface
{
    protected $_parameters  = null;
    
    public function __construct( $params = null ) 
    {
        if( null !== $params ) {
            $this->setParams( $params );
        }
    }
    
    public function setParams( $params ) 
    {
        if( is_array( $params ) ) {
            $params = new Parameters( $params );
        } else if( !( $params instanceof Parameters ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams(  ) : Parameters
    {
        if( null === $this->_parameters ) {
            $this->setParams( new Parameters( ) );
        }
        
        return $this->_parameters;
    }
    
    public function setController( string $controller ) 
    {
        $this->getParams( )->set( 'controller', $controller );
        return $this;
    }
    
    public function getController( string $default = null ): string 
    {
        return (string) $this->getParams( )->get( 'controller', $default );
    }
    
    public function setAction( string $action ) 
    {
        $this->getParams( )->set( 'action', $action );
        return $this;
    }
    
    public function getAction( string $default = null ): string 
    {
        return (string) $this->getParams( )->get( 'action', $default );
    }
    
    public function setPattern( string $pattern ) 
    {
        $this->getParams( )->set( 'pattern', $pattern );
        return $this;
    }
    
    public function getPattern( string $default = null ): string 
    {
        return (string) $this->getParams( )->get( 'pattern', $default );
    }
    
    public function setMethod( string $method ) 
    {
        $this->getParams( )->set( 'method', $method );
        return $this;
    }
    
    public function getMethod( string $default = null ): string 
    {
        return (string) $this->getParams( )->get( 'method', $default );
    }
    
    public function setSubdomain( string $subdomain ) 
    {
        $this->getParams( )->set( 'subdomain', $subdomain );
        return $this;
    }
    
    public function getSubdomain( string $default = null ): string 
    {
        return (string) $this->getParams( )->get( 'subdomain', $default );
    }
    
    public function setDomain( string $domain ) 
    {
        $this->getParams( )->set( 'domain', $domain );
        return $this;
    }
    
    public function getDomain( string $default = null ): string 
    {
        return (string) $this->getParams( )->get( 'domain', $default );
    }
    
    public function setLanguagePattern( string $pattern )
    {
        $this->getParams( )->set( 'languagePattern', $pattern );
        return $this;
    }
    
    public function getLanguagePattern( string $default = '' )
    {
        return (string) $this->getParams( )->get( 'languagePattern', $default );
    }
    
    private function matchPattern( $pattern, $string, &$params = array( )  )
    {
        if( $pattern == $string ) {
            return true;
        }
        
        $found = array( );

        if( !preg_match( '#^' . $pattern . '$#i', $string, $found ) ) {
            return false;
        }

        foreach( $found as $key => $value ) {
            if( !is_numeric( $key ) ) {
                $params[ $key ] = $value;
            }
        }
        
        return true;
        
    }
    
    public function match( HttpRequest $request )
    {
        if( ( $this->getSubdomain( ) && ( false === $this->matchPattern( $this->getSubdomain( ) , $request->getSubdomain( ), $params ) ) ) ||
            ( $this->getDomain( ) && ( false === $this->matchPattern( $this->getDomain( ) , $request->getDomain( ), $params ) ) ) ||
            ( $this->getMethod( ) && ( false === $request->isMethod( $this->getMethod( ) ) ) ) ) {
            return false;
        }
 
        $uri = rtrim( strtok( $request->getUri( ), '?' ), '/' );
        
        if( empty( $uri ) ) {
            $uri = '/';
        }
        
        if( ( $script_name = $request->getScriptName() ) && $script_name !== '' ) {
            $uri = $script_name == $uri ? '/' : substr( $uri, strlen( $script_name ) );
        }
        
        $params = array(
            'controller' => $this->getController( ),
            'action' => $this->getAction(  )
        );

        if( ( !empty( $this->getLanguagePattern( ) ) && false !== $this->matchPattern( $this->getLanguagePattern( ) . $this->getPattern( ) , $uri, $params ) ) || false !== $this->matchPattern( $this->getPattern( ) , $uri, $params ) ) {
            return $params;
        }

        return false;
        
    }
    
    public static function fromArray( array $params ) : self
    {
        if( !isset( $params[ 'pattern' ] ) ) {
            throw new InvalidArgumentException( );
        }
        
        return new self( $params );
    }
}
