<?php

namespace Thunderion\Mvc\Router;

use Thunderion\Mvc\Router\RouteStackInterface;
use Thunderion\Mvc\Router\RouteInterface;
use Thunderion\Mvc\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Mvc\Exception\InvalidRouteType as InvalidRouteTypeException;
use Thunderion\Http\Request as HttpRequest;
use Thunderion\Mvc\Router\RouteMatch;
use Thunderion\Mvc\Exception\RouterReadOnly as RouterReadOnlyException;

use Thunderion\PriorityList;


class RouteStack implements RouteStackInterface
{
    protected $_routes      = null;
    protected $_read_only   = false;
    
    public function __construct( array $routes = null ) 
    {
        if( null !== $routes ) {
            $this->setRoutes( $routes );
        }
    }
    
    protected function setList( PriorityList $list ) : self
    {
        $this->_routes = $list;
        return $this;
    }
    
    protected function getList( ) : PriorityList
    {
        if( null === $this->_routes ) {
            $this->setList( new PriorityList( ) );
        }
        
        return $this->_routes;
    }
    
    public function routeFromArray( array $params ): RouteInterface 
    {
        $class = isset( $params['type'] ) && !empty( $params[ 'type' ] ) ? trim( $params['type'], '/' ) : 'Thunderion\Mvc\Router\Route';
        
        if( !class_exists( $class ) || !is_subclass_of( $class , 'Thunderion\Mvc\Router\RouteInterface' ) ) {
            throw new InvalidRouteTypeException( );
        }
        
        return $class::fromArray( $params );
    }
    
    public function addRoute( string $name, $route, int $priority = 0 )
    {
        if( $this->isReadOnly( ) ) {
            throw new RouterReadOnlyException( );
        }
        
        if( is_array( $route ) ) {
            $route = $this->routeFromArray( $route );
        } else if( !( $route instanceof RouteInterface ) ) {
            throw new InvalidArgumentException( );
        }

        $this->getList( )->insert( $name, $route, $priority );
        return $this;
    }
    
    public function addRoutes( array $routes )
    {
        foreach( $routes as $name => $route ) {
            $this->addRoute( $name , $route );
        }
        
        return $this;
    }
    
    
    public function setRoutes( array $routes )
    {
        if( $this->isReadOnly( ) ) {
            throw new RouterReadOnlyException( );
        }
        
        $this->getList( )->clear( );
        return $this->addRoutes( $routes );
    }
    
    public function removeRoute( string $name ) 
    {
        if( $this->isReadOnly( ) ) {
            throw new RouterReadOnlyException( );
        }
        
        
        $this->getList( )->remove( $name );
        return $this;
    }
    
    
    public function getRoute( string $name ) 
    {
        return $this->getList( )->get( $name, false );
    }
    
    public function getRoutes( ): array 
    {
        return $this->getList( )->toArray( );
    }
    
    public function clear( )
    {
        $this->getList( )->clear( );
        return $this;
    }
    
    
    public function match( HttpRequest $request )
    {
        if( $this->getList( )->isEmpty( ) ) {
            return false;
        }
        
        foreach( $this->getList( ) as $name => $route ) {
            if( false !== ( $params = $route->match( $request ) ) ) {
               return new RouteMatch( $name, $params );
            }
        }
        
        return false;
    }
    
    public function isReadOnly( bool $flag = null ) : bool
    {
        if( null !== $flag ) {
            $this->_read_only = $flag;
        }
        
        return $this->_read_only;
    }
    
    public static function fromArray( array $routes )
    {
        return new self( $routes );
    }
    
}

