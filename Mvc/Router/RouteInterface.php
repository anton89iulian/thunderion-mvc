<?php

namespace Thunderion\Mvc\Router;

use Thunderion\Http\Request as HttpRequest;

interface RouteInterface 
{
    public function setController( string $controller );
   
    public function getController( string $default = null ): string;

    public function setAction( string $action );

    public function getAction( string $default = null ): string;

    public function setPattern( string $pattern );

    public function getPattern( string $default = null )  : string;

    public function setMethod( string $method );

    public function getMethod( string $default = null )  : string;
    
    public function setDomain( string $domain );

    public function getDomain( string $default = null )  : string;

    public function setSubdomain( string $subdomain );

    public function getSubdomain( string $default = null )  : string;

    public function match( HttpRequest $request );
    
    public static function fromArray( array $params );
}
