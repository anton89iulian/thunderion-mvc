<?php

namespace Thunderion\Mvc\Router;

use Thunderion\Mvc\Router\RouteInterface;
use Thunderion\Http\Request as HttpRequest;

interface RouteStackInterface 
{
    public function addRoute( string $name, $route, int $priority = 0 );
    
    public function addRoutes( array $routes );
    
    public function setRoutes( array $routes );
    
    public function removeRoute( string $name );
    
    public function getRoutes(  ): array;
    
    public function getRoute( string $name );
    
    public function isReadOnly( bool $flag = null ) : bool;
    
    public function match( HttpRequest $request );
    
    public function routeFromArray( array $params ) : RouteInterface;
    
    public function clear( ); 
}
