<?php

namespace Thunderion\Service;

use Thunderion\Parameters;
use Thunderion\Service\ManagerInterface as ServiceManagerInterface;

interface ServiceInterface
{
    
    public function setServiceManager( ServiceManagerInterface $manager );
    
    public function getServiceManager( ) : ServiceManagerInterface;
    
    public function setParams( $params );
    
    public function getParams( ) : Parameters;
    
    public function setParam( string $key, $value );
    
    public function getParam( string $key, $default = null );
    
}
