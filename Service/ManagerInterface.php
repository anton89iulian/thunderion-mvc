<?php

namespace Thunderion\Service;

interface ManagerInterface
{   
    public function validate( $service );
    
    public function set( string $name, $service );
    
    public function get( string $name, $args = null );
    
    public function remove( string $name );
    
    public function clear( );
}

