<?php

namespace Thunderion\Service;

use Thunderion\Service\ManagerInterface as ServiceManagerInterface;
use Thunderion\Service\ServiceInterface;
use Thunderion\Service\Exception\ServiceNotFound as ServiceNotFoundException;
use Thunderion\Service\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Parameters;

abstract class Manager implements ServiceManagerInterface
{
    protected $_services    = array( );
    protected $_namespace   = null;
    
    public function setNamespace( string $namespace )
    {
        $this->_namespace = rtrim( $namespace, '\\' );
        return $this;
    }
    
    public function getNamespace(  ) : string
    {
        return $this->_namespace;
    }
            
    public function serviceToClass( string $service ) : string
    {
        $service = str_replace( array( '.', '-', '_' ), ' ', $service );
        $service = ucwords( $service );
        return str_replace( ' ', '', $service );
    }
    
    abstract public function validate( $service );
    
    public function set( string $name, $service, bool $filterName = true )
    {
        if( $filterName ) {
            $name = $this->serviceToClass( $name );
        }
        
        $this->validate( $service );
        $this->_services[ $name ] = $service;
        
        return $this;
    }
    
    public function get( string $name, $args = null ) 
    {
        $name = $this->serviceToClass( $name );
        
        if( isset( $this->_services[ $name ] ) ) {
            return $this->_services[ $name ];
        }
        
        
        $class = $this->getNamespace( ) . '\\' . $name;

        if( !class_exists( $class ) ) {
            throw new ServiceNotFoundException( $class );
        }
        
        $object = new $class;
        
        $this->validate( $object );
        
        if( $object instanceof ServiceInterface ) {
            $object->setServiceManager( $this );
        }
        
        if( null !== $args ) {
            if( is_array( $args ) ) {
                $args = new Parameters( $args );
            } else if( !( $args instanceof Parameters ) ) {
                throw new InvalidArgumentException( 'Args are invalid; must extend Thunderion\Parameters' );
            }
            
            $object->setParams( $args );
        }
        
        $this->set( $name, $object, false );
        
        return $this->_services[ $name ]; 
    }
    
    public function __get( $name )
    {
        return $this->get( $name );
    }
    
    public function __call( $name, $args )
    {
        return $this->get( $name, $args );
    }
    
    public function remove( string $name )
    {
        $name = $this->serviceToClass( $name );
        
        if( isset( $this->_services[ $name ] ) ) {
            unset( $this->_services[ $name ] );
        }
        
        return $this;
    }
    
    public function clear( ) 
    {
        $this->_services = array( );
        return $this;
    }
    
}
