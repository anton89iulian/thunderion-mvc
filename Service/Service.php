<?php

namespace Thunderion\Service;

use Thunderion\Parameters;
use Thunderion\Service\ManagerInterface as ServiceManagerInterface;
use Thunderion\Service\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\Service\ServiceInterface;


abstract class Service implements ServiceInterface
{
    protected $_service_manager = null;
    protected $_parameters      = null;
    
    public function setServiceManager( ServiceManagerInterface $manager )
    {
        $this->_service_manager = $manager;
        return $this;
    }
    
    public function getServiceManager(  ) : ServiceManagerInterface
    {
        return $this->_service_manager;
    }
    
    public function setParams( $params )
    {
        if( is_array( $params ) ) {
            $params = new Parameters( $params );
        } else if( !( $params instanceof Parameters ) ) {
            throw new InvalidArgumentException( );
        }
        
        $this->_parameters = $params;
        return $this;
    }
    
    public function getParams(  ) : Parameters
    {
        if( null === $this->_parameters ) {
            $this->setParams( new Parameters( ) );
        }
        
        return $this->_parameters;
    }
    
    public function getParam( string $key, $default = null )
    {
        return $this->getParams( )->get( $key, $default );
    }
    
    public function setParam( string $key, $value )
    {
        $this->getParams( )->set( $key, $value );
        return $this;
    }
}
