<?php

namespace Thunderion\i18n\Reader;

use Thunderion\i18n\Reader\ReaderInterface;
use Thunderion\i18n\Exception\ErrorReadingFile as ErrorReadingFileException;
use Thunderion\i18n\Exception\FileNotFound as FileNotFoundException;
use Thunderion\i18n\Exception\InvalidArgument as InvalidArgumentException;

class Mo implements ReaderInterface
{
    public function fromFile( string $path ) : array
    {
        if( !file_exists( $path ) ) {
            throw new FileNotFoundException( $path );
        }
        
        if( !is_file( $path ) || !is_readable( $path ) || false === ( $resource = fopen( $path, 'rb' ) ) ) {
            throw new ErrorReadingFileException( $path );
        }
        
        $magic = fread( $resource, 4 );
        $array = array( );
        $littleEndian = false;
        
        if( "\x95\x04\x12\xde" == $magic ) {
            $littleEndian = false;
        } else if( "\xde\x12\x04\x95" == $magic ) {
            $littleEndian = true;
        } else {
            fclose( $resource );
            throw new InvalidArgumentException( 'Not a valid mo file' );
        }
        
        $majorRevision = ( $this->readInteger( $resource, $littleEndian ) >> 16 );
        
        if ( $majorRevision !== 0 && $majorRevision !== 1 ) {
            fclose( $resource );
            throw new InvalidArgumentException( 'has an unknown major revision' );
        }
        
        $numStrings                   = $this->readInteger( $resource, $littleEndian );
        $originalStringTableOffset    = $this->readInteger( $resource, $littleEndian );
        $translationStringTableOffset = $this->readInteger( $resource, $littleEndian );
        
        fseek( $resource, $originalStringTableOffset );
        $originalStringTable = $this->readIntegerList( 2 * $numStrings, $resource, $littleEndian );
        fseek( $resource, $translationStringTableOffset );
        $translationStringTable = $this->readIntegerList( 2 * $numStrings, $resource, $littleEndian );
        
        for ( $current = 0; $current < $numStrings; $current++ ) {
            $sizeKey                 = $current * 2 + 1;
            $offsetKey               = $current * 2 + 2;
            $originalStringSize      = $originalStringTable[$sizeKey];
            $originalStringOffset    = $originalStringTable[$offsetKey];
            $translationStringSize   = $translationStringTable[$sizeKey];
            $translationStringOffset = $translationStringTable[$offsetKey];

            $originalString = array('');
            if ( $originalStringSize > 0 ) {
                fseek( $resource, $originalStringOffset );
                $originalString = explode("\0", fread( $resource, $originalStringSize ) );
            }

            if ( $translationStringSize > 0 ) {
                fseek( $resource, $translationStringOffset );
                $translationString = explode("\0", fread( $resource, $translationStringSize ) );

                if ( count( $originalString ) > 1 && count( $translationString ) > 1 ) {
                    $array[$originalString[0]] = $translationString;

                    array_shift( $originalString );

                    foreach ( $originalString as $string ) {
                        if ( !isset( $array[$string] ) ) {
                            $array[ $string ] = '';
                        }
                    }
                } else {
                    $array[ $originalString[0] ] = $translationString[0];
                }
            }
        }
        
        fclose( $resource );
        
        return $array;
    }
    
    public function fromString( string $string ) : array
    {
         throw new InvalidArgumentException( );
    }
    
    public function readInteger( $fileResource, bool $littleEndian = true  ) : int
    {
        $result = $littleEndian ? unpack( 'Vint' , fread( $fileResource, 4 ) ) : unpack( 'Nint', fread( $fileResource, 4 ) );
        return $result['int'];
    }
    
    public function readIntegerList( int $num, $fileResource, bool $littleEndian = true ) : array
    {
        return $littleEndian ? unpack( 'V' . $num, fread( $fileResource, 4 * $num ) ) : unpack( 'N' . $num, fread( $fileResource, 4 * $num ) );
    }
}
