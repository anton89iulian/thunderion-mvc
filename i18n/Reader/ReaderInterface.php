<?php

namespace Thunderion\i18n\Reader;

interface ReaderInterface 
{
    public function fromFile( string $path ) : array;
    public function fromString( string $string ) : array;
}
