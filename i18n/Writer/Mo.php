<?php

namespace Thunderion\i18n\Writer;
use Thunderion\i18n\Writer\WriterInterface;
use Thunderion\i18n\Exception\InvalidArgument as InvalidArgumentException;
use Thunderion\i18n\Exception\ErrorWritingFile as ErrorWritingFileException;

class Mo implements WriterInterface
{
    public function toFile( array $data, string $path ) : string 
    {
        if( empty( $path ) || ( file_exists( $path ) && !is_writable( $path ) ) ) {
            throw new ErrorWritingFileException( $path );
        }
        
        $string = $this->toString( $data );
        
        if( false === file_put_contents( $path, $string ) ) {
            throw new ErrorWritingFileException( $path );
        }
        
        return $string;
    }
    
    public function toString( array $data ): string 
    {
        $moString = '';
        $offsets = array( );
        $ids = '';
        $strings = '';
        
        foreach( $data as $entry ) {
            if( !isset( $entry[ 'msgid' ] ) ) {
                throw new InvalidArgumentException( );
            }
            
            $id = $entry[ 'msgid' ];
            $str = "";
            
            if( isset( $entry[ 'msgid_plural' ] ) ) {
                $id .= "\x00" . $entry[ 'msgid_plural' ];
            }
            
            if( isset( $entry['msgctxt'] ) ) {
                $id = $entry[ 'msgctxt' ] . "\x04" . $id;
            }
            
            if( isset( $entry['msgstr'] ) && is_array( $entry['msgstr'] ) ) {
               $str = implode( "\x00", $entry['msgstr'] );
            } else {
                $str = $entry['msgstr'];
            }
            
            $offsets[] = array( strlen( $ids ), strlen( $id ), strlen( $strings ), strlen( $str ) );
            $ids .= $id . "\x00";
            $strings .= $str . "\x00";
        }
        
        $key_start = 7 * 4 + sizeof( $data ) * 4 * 4;
        $value_start = $key_start + strlen( $ids );
        
        $key_offsets = array( );
	$value_offsets = array( );
        
        foreach( $offsets as $v ) {
            list ( $o1, $l1, $o2, $l2 ) = $v;
            
            $key_offsets[ ] = $l1;
            $key_offsets[ ] = $o1 + $key_start;
            $value_offsets[ ] = $l2;
            $value_offsets[ ] = $o2 + $value_start;
        }
        
        $offsets = array_merge( $key_offsets, $value_offsets );
        
        $moString .= pack( 'Iiiiiii', 0x950412de, 0, sizeof( $data ), 7 * 4, 7 * 4 + sizeof( $data ) * 8, 0, $key_start  );
        
        foreach( $offsets as $offset ) {
            $moString .= pack( 'i', $offset );
        }
        
        $moString .= $ids . $strings;
        
        return $moString;
    }
}
