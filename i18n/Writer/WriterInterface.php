<?php

namespace Thunderion\i18n\Writer;

interface WriterInterface 
{
    public function toFile( array $data, string $path ) : string;
    public function toString( array $data ) : string;
}
