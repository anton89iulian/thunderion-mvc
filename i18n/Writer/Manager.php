<?php

namespace Thunderion\i18n\Writer;

use Thunderion\Service\Manager as AbstractManager;
use Thunderion\i18n\Writer\WriterInterface;
use Thunderion\i18n\Exception\InvalidArgument as InvalidArgumentException;

class Manager extends AbstractManager
{
    public function __construct( ) 
    {
        $this->setNamespace( __NAMESPACE__ );
    }
    
    public function validate( $service ) 
    {
        if( $service instanceof WriterInterface ) {
            return null;
        }
        
        throw new InvalidArgumentException( sprintf( 'Service is invalid; must implement %s\WriterInterface', __NAMESPACE__ ) );
    }
}
