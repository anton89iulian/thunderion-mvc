<?php


namespace Thunderion\i18n\Translator;

interface TranslatorInterface 
{
   public function setLocale( string $locale );
   public function setFileType( string $file_type );
   public function setBaseDir( string $path );
   public function translate( string $message, string $locale = null ) : string;
   public function isActive( bool $flag = null );
   public function setDefaultLanguage( string $language_code );
   public function getDefaultLanguage(  ) : string;
}
