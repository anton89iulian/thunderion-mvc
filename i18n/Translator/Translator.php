<?php

namespace Thunderion\i18n\Translator;

use Thunderion\i18n\Reader\Manager as ReaderManager;
use Thunderion\i18n\Reader\ReaderInterface;
use Thunderion\i18n\Exception\UnsupoportedFileExtension as UnsupoportedFileExtensionException;
use Thunderion\i18n\Exception\FileNotFound as FileNotFoundException;
use Thunderion\i18n\Translator\TranslatorInterface;

class Translator implements TranslatorInterface
{
    protected $_file_type = '';
    protected $_locale = '';
    protected $_default_language = '';
    protected $_base_dir;
    protected $_languages = array( );
    protected $_extensions= array(
        'mo' => 'mo'
    );
    protected $_reader_manager = null;
    protected $_active = false;
    
    public function isActive( bool $flag = null ) 
    {
        if( null !== $flag ) {
            $this->_active = $flag;
            return $this;
        }
        
        return $this->_active;
    }
    
    public function setLocale( string $locale )
    {
        $this->_locale = $locale;
        return $this;
    }
    
    public function getLocale( ) : string
    {
        return $this->_locale;
    }
    
    public function setDefaultLanguage( string $language_code )
    {
        $this->_default_language = $language_code;
        return $this;
    }
    
    public function getDefaultLanguage(  ) : string
    {
        return $this->_default_language;
    }
    
    public function setFileType( string $file_type )
    {
        $this->_file_type = $file_type;
        return $this;
    }
    
    public function getFileType( ) : string
    {
        return $this->_file_type;
    }
    
    public function setBaseDir( string $path )
    {
        $this->_base_dir = rtrim( $path, '\/' );
        return $this;
    }
    
    public function getBaseDir( ) : string
    {
        return $this->_base_dir;
    }
    
    public function setReaderManager( ReaderManager $manager ) : self
    {
        $this->_reader_manager = $manager;
        return $this;
    }
    
    public function getReaderManager( ) : ReaderManager
    {
        if( null === $this->_reader_manager ) {
            $this->setReaderManager( new ReaderManager( ) );
        }
        
        return $this->_reader_manager;
    }
    
    public function setReader( string $extension, ReaderInterface $reader ) : self
    {
        $this->_extensions[ $extension ] = $reader;
        return $this;
    }
    
    public function getReader( string $extension ) : ReaderInterface
    {
        if( !isset( $this->_extensions[ $extension ] ) ) {
            throw new UnsupoportedFileExtensionException( );
        }
        
        if( is_string( $this->_extensions[ $extension ] ) ) {
            $this->setReader( $extension, $this->getReaderManager( )->get( $this->_extensions[ $extension ] ) );
        }
        
        return $this->_extensions[ $extension ];
    }
    
    public function toArray( )
    {
        if( !isset( $this->_languages[ $this->getLocale( ) ] ) ) {
            $path = $this->getBaseDir( ) . '/' . $this->getLocale( ) . '.' . $this->getFileType( );
            
            if( !file_exists( $path ) ) {
                return array( );
            }
            
            $this->_languages[ $this->getLocale( ) ] = $this->getReader( $this->getFileType( ) )->fromFile( $path );
        }
        
        $this->_languages[ $this->getLocale( ) ];
    }
    
    
    public function translateMessage( string $message, string $locale, $default = null )
    {
        if( !$this->isActive( ) || $locale == $this->getDefaultLanguage( ) ) {
            return $message;
        }
        
        if( !isset( $this->_languages[ $locale ] ) ) {
            $path = $this->getBaseDir( ) . '/' . $locale . '.' . $this->getFileType( );
            
            if( !file_exists( $path ) ) {
                return $default;
            }
            
            $this->_languages[ $locale ] = $this->getReader( $this->getFileType( ) )->fromFile( $path );
        }
        
        if( !isset( $this->_languages[ $locale ][ $message ] ) ) {
            return $default;
        }
        
        if( is_array( $this->_languages[ $locale ][ $message ] ) ) {
            return reset( $this->_languages[ $locale ][ $message ] );
        }
        
        return $this->_languages[ $locale ][ $message ];
    }
    
    public function translate( string $message, string $locale = null ) : string
    {
        if( null === $locale ) {
            $locale = $this->getLocale( );
        }
        
        return $this->translateMessage( $message, $locale, $message );
    }
    
    public function __invoke( string $message, string $locale = null )
    {
        return $this->translate( $message, $locale );
    }
    
}
