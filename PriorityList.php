<?php

namespace Thunderion;

class PriorityList implements \Iterator, \Countable
{
    protected $_sorted = false;
    
    protected $_serial = 0;
    
    protected $_items = [];
    
    protected $_is_lifo = 1;
    
    public function __construct( array $array = null ) 
    {
        if( null !== $array ) {
            foreach( $array as $key => $value ) {
                $this->insert( $key, $value );
            }
        }
    }
    
    public function insert( string $key, $value, int $priority = 0 )
    {
        $this->_sorted = false;
        
        $this->_items[ $key ] = (object) array(
            'value' => $value,
            'priority' => $priority,
            'serial' => $this->_serial++
        );
        
        return $this;
    }
    
    public function has( string $key ) : bool
    {
        return array_key_exists( $key , $this->_items );
    }
    
    public function setPriority( string $key, int $priority )
    {
        if( $this->has( $key ) ) {
            $this->item[ $key ]->priority = $priority;
            $this->_sorted = false;
        }
        
        return $this;
    }
    
    public function get( $key, $default = null ) 
    {
        return $this->has( $key ) ? $this->_items[ $key ]->value : $default;
    }
    
    public function remove( $key )
    {
        if( $this->has( $key )) {
            unset( $this->_items[ $key ] );
        }
        return $this;
    }
    
    public function clear( )
    {
        $this->_items   = [];
        $this->_sorted  = false;
        $this->_serial  = 0;
    }
    
    public function isLifo( bool $flag = null ) : bool
    {
        if( null !== $flag ) {
            $isLifo = true === $flag ? 1 : -1;
        
            if( $isLifo !== $this->_is_lifo ) {
                $this->_is_lifo = $isLifo;
                $this->_sorted = false;
            }
        }
        
        return 1 === $this->_is_lifo;
    }
    
    public function isSorted( ) : bool
    {
        return $this->_sorted;
    }
    
    protected function sort( )
    {
        if( !$this->isSorted( ) ) {
            uasort( $this->_items, array( $this, 'compare' ) );
            $this->_sorted = true;
        }
        return $this;
    }
    
    protected function compare( \stdClass $a, \stdClass $b ) : int
    {
        if( $a->priority === $b->priority ) {
            return ( $a->serial > $b->serial ? -1 : 1 ) * $this->_is_lifo;
        }
        
        return $a->priority > $b->priority ? -1 : 1;
    }
    
    public function isEmpty( )
    {
        return empty( $this->_items );
    }
    
    public function key( )
    {
        $this->sort( ); 
        return key( $this->_items );
    }
    
    public function rewind( )
    {
        $this->sort( ); 
        return reset( $this->_items );
    }
    
    public function valid( ) : bool
    {
        $this->sort( ); 
        return current( $this->_items ) !== false;
    }
    
    
    public function current( )
    {
        $this->sort( ); 
        return ( $item = current( $this->_items ) ) ? $item->value : false;
    }

    public function next( )
    {
        return ( $item = next( $this->_items ) ) ? $item->value : false;
    }
    
    public function count( ) : int
    {
        return count( $this->_items );
    }
    
    public function toArray( ) : array
    {
        $this->sort( );
        $array = array( );
        
        foreach( $this->_items as $key => $item ) {
            $array[ $key ] = $item->value;
        }
        
        return $array;
    }
    
    public function __set( $key, $value )
    {
        $this->insert( $key, $value );
    }
    
    public function __get( $key )
    {
        return $this->get( $key );
    }
    
    public function __isset( $key ) 
    {
        return $this->has( $key );
    }
    
}

